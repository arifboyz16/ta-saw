const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const express = require("express")
const helmet = require('helmet')
const http = require('http')
const path = require('path')
const cors = require('cors')

require('dotenv').config({
  path: path.resolve(process.cwd(), '.env'),
})
const db = require("./utils/database").mongoURI

const app = express()

app.use(cors())
app.use(helmet())
app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

// mongoose
//     .connect(db)
//     .then(() => console.log("mongoDB Connected"))
//     .catch((err) => console.log(err));
const connectWithRetry = function() {
  return mongoose.connect(db, function(err) {
    if (err) {
      console.error('Failed to connect to mongo on startup - retrying in 3 sec', err);
      setTimeout(connectWithRetry, 3000);
    } else {
      console.log("mongoDB Connected")
    }
  });
};
connectWithRetry();

mongoose.Promise = global.Promise

app.set('models', mongoose.models)

// Import modules
const auth = require('./modules/auth')
const user = require('./modules/users')
const customer = require('./modules/customer')
const vehicle = require('./modules/vehicle')
const leasing = require('./modules/leasing')
const creditDecision = require('./modules/creditDecision')
const sawSimulation = require('./utils/saw')

//routes
app.use(express.static('public'))
app.use('/api/auth', auth)
app.use('/api/user', user)
app.use('/api/customer', customer)
app.use('/api/vehicle', vehicle)
app.use('/api/leasing', leasing)
app.use('/api/credit-decision', creditDecision)
app.use('/api/perhitungan', sawSimulation)

// Create http server
const server = http.createServer(app)

const port = process.env.PORT || 5000;

server.listen(port, () => console.log("server running on port "+port));
