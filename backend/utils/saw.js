const _ = require('lodash')
const express = require('express')
const criteria = require('./criteria')
const dataPilihan = require('./data')
const Pilihan = dataPilihan.data
const authenticate = require('../modules/auth/methods/authenticate')

function getMinMax(value) {
	max = Math.max(value.keuangan, value.rumah, value.garasi, value.kredit)
	return max
}

function normalisasi(matrix, maxmin) {
	matrix.keuangan = matrix.keuangan / maxmin;
	matrix.rumah = matrix.rumah / maxmin;
	matrix.garasi = matrix.garasi / maxmin;
	matrix.kredit = matrix.kredit / maxmin;

	return matrix;
}

function hitungPeringkat(nilai) {
	const total = (nilai.keuangan * criteria.bobot) + (nilai.rumah * criteria.bobot) + (nilai.garasi * criteria.bobot) + (nilai.kredit * criteria.bobot);
	let state = 'Approve'
	if (total <= 2) {
		state = 'Not Approve'
	}

	return state;
}

function debRatio (value) {
	let total
	let val
	total = (value.down_payment/((value.earning+value.other_income)-value.other_payment)*100)
	total = 100-total
	if(total < 30){
		val = 0
	} else if (total > 30 ){
		val = 2
	} else if (total > 41 ){
		val = 4
	} else if (total > 60 ){
		val = 6
	}
	return val
}

function valHouse (value) {
	let val
	if(value.house_status === 'join'){
		val = 1
	} else if (value.house_status === 'rent'){
		val = 2
	} else if (value.house_status === 'owned'){
		val = 3
	} 
	return val
}

function valGarage (value) {
	let val
	if(value.house_garage === 'nogarage'){
		val = 1
	} else if (value.house_garage === 'rent'){
		val = 2
	} else if (value.house_garage === 'owned'){
		val = 3
	}
	return val
}

function valCredit (value) {
	let val
	if(value.credit_history === 'bad'){
		val = 0
	} else if (value.credit_history === 'surveillance'){
		val = 1
	} else if (value.credit_history === 'newcomer'){
		val = 2
	} else if (value.credit_history === 'good'){
		val = 3
	}
	return val
}
function perhitunganSimulasiKredit(value) {
	let total
	total = ((value.harga_kendaraan+((value.harga_kendaraan*value.leasing)/100))-value.downpayment)
	total = total/value.periode
	return Math.floor(total)
}

getRecomendationSAW = async (req, res) => {
	const ratioVal = await debRatio(req.body)
	const houseVal = await valHouse(req.body)
	const garageVal = await valGarage(req.body)
	const creditVal = await valCredit(req.body)
    const data = [{
      keuangan : ratioVal,
      rumah : houseVal,
      garasi: garageVal,
      kredit : creditVal
    }]
	const getNilaiBobot = data
	const normalisasiNilai = _.map(getNilaiBobot, nilai => normalisasi(nilai, getMinMax(nilai)))
	const hitungBobotPeringkat = _.map(normalisasiNilai, nilai => hitungPeringkat(nilai))
	res.send(hitungBobotPeringkat);
};

creditSimulation = (req, res) => {
	const getNilaiBobot = [req.body]
	const nilai = _.map(getNilaiBobot, nilai => perhitunganSimulasiKredit(nilai))
	res.send(nilai)
};

const router = express.Router()
router.post('/saw-simulation', authenticate, getRecomendationSAW)
router.post('/credit-simulation', authenticate, creditSimulation)
module.exports = router

