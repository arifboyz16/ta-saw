const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import model
const Customer = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    let results

    const { nama, type_user, search} = req.query // eslint-disable-line
    const page = parseInt(req.query.page) - 1 || 0 // eslint-disable-line
    const pageSize = parseInt(req.query.pageSize) || 5 // eslint-disable-line
    const skip = page * pageSize

    const rules = [
      {
        '$match': {
          is_active: true,
        },
      },
      {
        '$project': {
          _id: 0,
          id: '$_id',
          nik: 1,
          name: 1,
          address: 1,
          handphone: 1,
        },
      },
    ]

    if (search) {
      const terms = new RegExp(search, 'i')

      rules.push({
        '$match': {
          'nik': {
            '$regex': terms,
          },
        },
      })
    }

    if (nama) {
      const nTerms = new RegExp(nama, 'i') 
 
      rules.push({ 
        '$match': { 
          'name': { 
            '$regex': nTerms, 
          }, 
        }, 
      })
    }
    
    const countRules = [
      ...rules,
      {
        '$group': { _id: null, rows: { '$sum': 1 } },
      },
      {
        '$project': {
          rows: 1,
        },
      },
    ]

    const total = await Customer.aggregate(countRules)

    results = await Customer
      .aggregate(rules)
      .sort({ name: 1 })
      .skip(skip)

    res.send({
      total: total.length > 0 ? total[0].rows : 0,
      page: page + 1,
      pageSize,
      results,
    })
  } catch (error) {
    console.error(error) // eslint-disable-line
    res.status(500).send({
      code: errors.serverError.code,
      message: errors.serverError.message,
    })
  }
}
