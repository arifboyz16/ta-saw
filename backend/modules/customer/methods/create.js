const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import model
const Customer = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { _id: created_by } = req.user
    const {
      nik = null,
      name = null,
      address = null,
      handphone = null,
      tax_payer_id = null,
      earning = null
    } = req.body
    const data = {
      nik,
      name,
      address,
      handphone,
      tax_payer_id,
      earning,
      created_by,
      created_date: Date.now(),
    }

    await Customer.create(data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
