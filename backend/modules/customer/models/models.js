const mongoose = require('mongoose')
const uuidv4 = require('uuid/v4')

const Schema = mongoose.Schema

const Customer = new Schema({
  _id: {
    type: String,
    default: uuidv4,
  },
  nik: String,
  name: String,
  address: String,
  handphone: String,
  tax_payer_id: String,
  earning: {
    type: Number,
    required: false,
    default: true,
  },
  is_active: {
    type: Boolean,
    required: false,
    default: true,
  },
  created_by: {
    type: String,
    required: false,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  updated_by: {
    type: String,
    required: false,
    default: null,
  },
  updated_date: {
    type: Date,
    required: false,
    default: null,
  },
})

Customer.index({
  nik: 1,
  name: 1,
})

module.exports = mongoose.models.Customer || mongoose.model('Customer', Customer)
