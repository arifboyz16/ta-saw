const jwt = require('jsonwebtoken')
const { errors, APIError } = require('../../../utils/exceptions')

// Import model
const User = require('../../users/models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { email, password } = req.body
    let user
    user = await User.findOne({ email, is_active: true })
    if (user === null){
      user = await User.findOne({ username: email, is_active: true })
    }

    if (!user) {
      throw new APIError(errors.wrongCredentials)
    }

    const authenticated = await user.comparePassword(password)

    if (!authenticated) {
      throw new APIError(errors.wrongCredentials)
    }

    const { 
      _id,
      email: uEmail,
      nip,
      first_name,
      last_name,
      posisi,
      role
    } = user
    const fullName = `${first_name} ${last_name}`
    // eslint-disable-next-line
    const token = jwt.sign({ _id, email: uEmail, nip, nama: fullName, posisi, role }, process.env.JWT_SECRET_KEY, {
      expiresIn: 86400,
    })

    res.send({
      token,
      created: new Date(),
      user: {
        id: _id,
        email,
        nama: fullName,
        nip,
        posisi,
        role,
      },
    })

  } catch (error) {
    console.error(error)
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
