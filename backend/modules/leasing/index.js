const express = require('express')
// Import middleware
const authenticate = require('../auth/methods/authenticate')

const router = express.Router()

// Import methods
const list = require('./methods/list')
const create = require('./methods/create')
const update = require('./methods/update')
const remove = require('./methods/delete')
const detail = require('./methods/detail')

router.get('/', authenticate, list)
router.post('/', authenticate, create)
router.put('/:id', authenticate, update)
router.delete('/:id', authenticate, remove)
router.get('/:id', authenticate, detail)

module.exports = router
