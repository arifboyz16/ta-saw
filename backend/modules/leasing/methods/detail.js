const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import model
const Leasing = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { id: _id } = req.params

    const data = await Leasing.findOne({ _id }).lean()

    data.id = data._id

    delete data._id

    res.send(data)
  } catch (error) {
    console.error(error) // eslint-disable-line
    res.status(500).send({
      code: errors.serverError.code,
      message: errors.serverError.message,
    })
  }
}
