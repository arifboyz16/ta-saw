const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import Model
const Leasing = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { _id: created_by } = req.user

    const {
      leasing_code = null,
      leasing_name = null,
      leasing_rate = null,
    } = req.body
    const data = {
      leasing_code,
      leasing_name,
      leasing_rate,
      created_by,
      created_date: Date.now(),
    }

    await Leasing.create(data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
