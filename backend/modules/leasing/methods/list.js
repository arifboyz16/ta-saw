const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import model
const Leasing = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    let results

    const { leasing_name, search } = req.query // eslint-disable-line
    const page = parseInt(req.query.page) - 1 || 0 // eslint-disable-line
    const pageSize = parseInt(req.query.pageSize) || 5 // eslint-disable-line
    const skip = page * pageSize

    let rules = [
      {
        '$project': {
          _id: 0,
          id: '$_id',
          leasing_code: 1,
          leasing_name: 1,
          leasing_rate: 1,
          created_date: 1,
          created_by: 1,
        },
      },
    ]


    if (search) {
      const terms = new RegExp(search, 'i')

      rules.push({
        '$match': {
          'leasing_name': {
            '$regex': terms,
          },
        },
      })
    }

    if (leasing_name) {
      const nTerms = new RegExp(leasing_name, 'i')

      rules.push({
        '$match': {
          'leasing_name': {
            '$regex': nTerms,
          },
        },
      })
    }


    const countRules = [
      ...rules,
      {
        '$group': { _id: null, rows: { '$sum': 1 } },
      },
      {
        '$project': {
          rows: 1,
        },
      },
    ]

    const total = await Leasing.aggregate(countRules)

    results = await Leasing
      .aggregate(rules)
      .sort({ parent: 1 })
      .skip(skip)
      .limit(pageSize)

    res.send({
      total: total.length > 0 ? total[0].rows : 0,
      page: page + 1,
      pageSize,
      results,
    })
  } catch (error) {
    console.error(error) // eslint-disable-line
    res.status(500).send({
      code: errors.serverError.code,
      message: errors.serverError.message,
    })
  }
}
