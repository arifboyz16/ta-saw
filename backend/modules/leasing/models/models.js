const mongoose = require('mongoose')
const uuidv4 = require('uuid/v4')

const Schema = mongoose.Schema

const Leasing = new Schema({
  _id: {
    type: String,
    default: uuidv4,
  },
  leasing_code: String,
  leasing_name: String,
  leasing_rate: Number,
  created_by: {
    type: String,
    required: false,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  updated_by: {
    type: String,
    required: false,
    default: null,
  },
  updated_date: {
    type: Date,
    required: false,
    default: null,
  },
})

Leasing.index({
  vehicle_code: 1,
  vehicle_name: 1,
})

module.exports = mongoose.models.Leasing || mongoose.model('Leasing', Leasing)
