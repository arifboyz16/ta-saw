const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const uuidv4 = require('uuid/v4')

const Schema = mongoose.Schema

const User = new Schema({
  _id: {
    type: String,
    default: uuidv4,
  },
  email: String,
  username: String,
  password: String,
  first_name: String,
  last_name: String,
  nrp: String,
  handphone: {
    type: String,
    required: false,
    default: null,
  },
  posisi: {
    type: String,
    required: false,
    default: null,
  },
  role: {
    type: String,
    required: true,
    default: 'user',
  },
  is_active: {
    type: Boolean,
    required: false,
    default: false,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  updated_date: {
    type: Date,
    required: false,
    default: null,
  },
  updated_by: {
    type: String,
    required: false,
    default: null,
  },
})

User.index({ role: 1 })

User.pre('save', async function (next) { // eslint-disable-line
  try {
    // Only hash the password if it has been modified (or is new)
    if (!this.isModified('password')) return next()

    // Generate salt
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(this.password, salt)
    this.password = hash
    return next()
  } catch (error) {
    next(error)
  }
})

User.methods.comparePassword = async function (password) { // eslint-disable-line
  const compare = await bcrypt.compare(password, this.password)

  return compare
}

module.exports = mongoose.models.User || mongoose.model('User', User)
