const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import Model
const User = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { id: _id } = req.params

    const {
      email = null,
      username = null,
      password = null,
      first_name = null,
      last_name = null,
      nip = null,
      nrp = null,
      handphone = null,
      posisi = null,
      type_user = null,
      role = null
    } = req.body

    const data = {
      email,
      username,
      password,
      first_name,
      last_name,
      nip,
      nrp,
      handphone,
      posisi,
      type_user,
      role
    }

    await User.findOneAndUpdate({ _id }, data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
