const mongoose = require('mongoose')
const uuidv4 = require('uuid/v4')

const Schema = mongoose.Schema

const Vehicle = new Schema({
  _id: {
    type: String,
    default: uuidv4,
  },
  vehicle_code: String,
  vehicle_type: String,
  vehicle_name: String,
  vehicle_price: Number,
  created_by: {
    type: String,
    required: false,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  updated_by: {
    type: String,
    required: false,
    default: null,
  },
  updated_date: {
    type: Date,
    required: false,
    default: null,
  },
})

Vehicle.index({
  vehicle_code: 1,
  vehicle_name: 1,
})

module.exports = mongoose.models.Vehicle || mongoose.model('Vehicle', Vehicle)
