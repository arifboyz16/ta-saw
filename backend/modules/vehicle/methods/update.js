const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import Model
const Vehicle = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { id: _id } = req.params
    const { _id: updated_by } = req.user

    const {
      vehicle_code = null,
      vehicle_type = null,
      vehicle_name = null,
      vehicle_price = null
    } = req.body

    const data = {
      vehicle_code,
      vehicle_type,
      vehicle_name,
      vehicle_price,
      updated_by,
      updated_date: Date.now(),
    }

    await Vehicle.findOneAndUpdate({ _id }, data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
