const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import model
const Vehicle = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { _id: created_by } = req.user
    const {
      vehicle_code = null,
      vehicle_type = null,
      vehicle_name = null,
      vehicle_price = null
    } = req.body
    const data = {
      vehicle_code,
      vehicle_type,
      vehicle_name,
      vehicle_price,
      created_by,
      created_date: Date.now(),
    }

    await Vehicle.create(data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
