const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import Model
const CreditDecision = require('../models/models')
const Customer = require('../../customer/models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const { id: _id } = req.params
    const { _id: updated_by } = req.user

    const {
      customer_id = null,
      name = null,
      address = null,
      handphone = null,
      earning = null,
      house_status = null,
      other_income = null,
      house_garage = null,
      other_payment = null,
      credit_history = null,
      down_payment = null,
      periode = null,
      decision = null
    } = req.body

    const data = {
      customer_id,
      name,
      address,
      handphone,
      earning,
      house_status,
      other_income,
      house_garage,
      other_payment,
      credit_history,
      down_payment,
      periode,
      decision,
      updated_by,
      updated_date: Date.now(),
    }

    if (customer_id) {
      const _customer = await Customer.findById(customer_id)

      if (_customer) {
        data.nik = _customer.nik
      }
    }

    await CreditDecision.findOneAndUpdate({ _id }, data)

    res.status(201).send({
      message: 'Input data successfull',
      data,
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
