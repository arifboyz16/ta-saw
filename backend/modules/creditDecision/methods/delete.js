const { errors, APIError } = require('../../../utils/exceptions') // eslint-disable-line

// Import Model
const CreditDecision = require('../models/models')

module.exports = async (req, res) => { // eslint-disable-line
  try {
    const {
      id: _id,
    } = req.params

    await CreditDecision.deleteOne({ _id })

    res.status(201).send({
      message: 'Delete data successfull',
    })
  } catch (error) {
    const { code, message, data } = error

    if (code && message) {
      res.status(code).send({
        code,
        message,
        data,
      })
    } else {
      res.status(500).send(errors.serverError)
    }
  }
}
