const mongoose = require('mongoose')
const uuidv4 = require('uuid/v4')

const Schema = mongoose.Schema

const CreditDecision = new Schema({
  _id: {
    type: String,
    default: uuidv4,
  },
  customer_id: String,
  nik: {
    type: String,
    required: false,
    default: true,
  },
  name: String,
  address: String,
  handphone: String,
  earning: {
    type: Number,
    required: false,
    default: true,
  },
  house_status: String,
  other_income: Number,
  house_garage: String,
  other_payment: Number,
  credit_history: String,
  down_payment: Number,
  periode: Number,
  decision: String,
  created_by: {
    type: String,
    required: false,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  updated_by: {
    type: String,
    required: false,
    default: null,
  },
  updated_date: {
    type: Date,
    required: false,
    default: null,
  },
})

CreditDecision.index({
  nik: 1,
  name: 1,
})

module.exports = mongoose.models.CreditDecision || mongoose.model('CreditDecision', CreditDecision)
