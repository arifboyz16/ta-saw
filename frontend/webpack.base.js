const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const { NODE_ENV } = process.env

module.exports = {
  entry: {
    app: path.resolve(process.cwd(), 'src/index.js'), // eslint-disable-line
  },
  resolve: {
    modules: ['src', 'node_modules'],
    extensions: ['.js', '.jsx', '.less', '.css'],
    // modules: [
    //   path.resolve(__dirname, 'src'),
    //   path.resolve(__dirname, 'node_modules'),
    // ],
    alias: {
      '@': path.resolve(process.cwd(), 'src'), // eslint-disable-line
    },
  },
  module: {
    rules: [
      {
        test: /\.(?:jpg|png|gif|svg)$/,
        loader: 'url-loader',
        options: { limit: 10000 },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(NODE_ENV),
      API_URL: JSON.stringify(process.env.API_URL),
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'public/index.html'),
      filename: 'index.html',
    }),
    // Optimize moment.js size
    // new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  ],
}
