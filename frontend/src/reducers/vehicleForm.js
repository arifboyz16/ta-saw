import {
  VEHICLEFORM_ENTER_RESET,
  VEHICLEFORM_EDITFORM_CHANGE,
  VEHICLEFORM_FETCHITEM_LOAD,
  VEHICLEFORM_FETCHITEM_SUCCEED,
  VEHICLEFORM_FETCHITEM_FAIL,
  VEHICLEFORM_CREATEITEM_LOAD,
  VEHICLEFORM_CREATEITEM_SUCCEED,
  VEHICLEFORM_CREATEITEM_FAIL,
  VEHICLEFORM_EDITITEM_LOAD,
  VEHICLEFORM_EDITITEM_SUCCEED,
  VEHICLEFORM_EDITITEM_FAIL,
  VEHICLEFORM_DELETEITEM_LOAD,
  VEHICLEFORM_DELETEITEM_SUCCEED,
  VEHICLEFORM_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case VEHICLEFORM_FETCHITEM_LOAD:
      return { ...state, isFetchItemLoading: true, fetchItemError: null }
    case VEHICLEFORM_FETCHITEM_SUCCEED:
      return { ...state, isFetchItemLoading: false, item: action.item }
    case VEHICLEFORM_FETCHITEM_FAIL:
      return { ...state, isFetchItemLoading: false, fetchItemError: action.fetchItemError }
    case VEHICLEFORM_ENTER_RESET:
      return {}
    case VEHICLEFORM_EDITFORM_CHANGE:
      return { ...state, formFieldValues: { ...state.formFieldValues, ...action.field } }
    case VEHICLEFORM_CREATEITEM_LOAD:
      return {
        ...state,
        isCreateItemLoading: true,
        isCreateItemSuccess: false,
        createItemError: null,
      }
    case VEHICLEFORM_CREATEITEM_SUCCEED:
      return { ...state, isCreateItemLoading: false, isCreateItemSuccess: true }
    case VEHICLEFORM_CREATEITEM_FAIL:
      return { ...state, isCreateItemLoading: false, createItemError: action.createItemError }
    case VEHICLEFORM_EDITITEM_LOAD:
      return {
        ...state,
        isEditItemLoading: true,
        isEditItemSuccess: false,
        editItemError: null,
      }
    case VEHICLEFORM_EDITITEM_SUCCEED:
      return { ...state, isEditItemLoading: false, isEditItemSuccess: true }
    case VEHICLEFORM_EDITITEM_FAIL:
      return { ...state, isEditItemLoading: false, editItemError: action.editItemError }
    case VEHICLEFORM_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case VEHICLEFORM_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case VEHICLEFORM_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
