import {
  CREDITDECISION_ENTER_RESET,
  CREDITDECISION_EDITFORM_CHANGE,
  CREDITDECISION_FETCHITEM_LOAD,
  CREDITDECISION_FETCHITEM_SUCCEED,
  CREDITDECISION_FETCHITEM_FAIL,
  CREDITDECISION_CREATEITEM_LOAD,
  CREDITDECISION_CREATEITEM_SUCCEED,
  CREDITDECISION_CREATEITEM_FAIL,
  CREDITDECISION_EDITITEM_LOAD,
  CREDITDECISION_EDITITEM_SUCCEED,
  CREDITDECISION_EDITITEM_FAIL,
  CREDITDECISION_DELETEITEM_LOAD,
  CREDITDECISION_DELETEITEM_SUCCEED,
  CREDITDECISION_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case CREDITDECISION_FETCHITEM_LOAD:
      return { ...state, isFetchItemLoading: true, fetchItemError: null }
    case CREDITDECISION_FETCHITEM_SUCCEED:
      return { ...state, isFetchItemLoading: false, item: action.item }
    case CREDITDECISION_FETCHITEM_FAIL:
      return { ...state, isFetchItemLoading: false, fetchItemError: action.fetchItemError }
    case CREDITDECISION_ENTER_RESET:
      return {}
    case CREDITDECISION_EDITFORM_CHANGE:
      return { ...state, formFieldValues: { ...state.formFieldValues, ...action.field } }
    case CREDITDECISION_CREATEITEM_LOAD:
      return {
        ...state,
        isCreateItemLoading: true,
        isCreateItemSuccess: false,
        createItemError: null,
      }
    case CREDITDECISION_CREATEITEM_SUCCEED:
      return { ...state, isCreateItemLoading: false, isCreateItemSuccess: true }
    case CREDITDECISION_CREATEITEM_FAIL:
      return { ...state, isCreateItemLoading: false, createItemError: action.createItemError }
    case CREDITDECISION_EDITITEM_LOAD:
      return {
        ...state,
        isEditItemLoading: true,
        isEditItemSuccess: false,
        editItemError: null,
      }
    case CREDITDECISION_EDITITEM_SUCCEED:
      return { ...state, isEditItemLoading: false, isEditItemSuccess: true }
    case CREDITDECISION_EDITITEM_FAIL:
      return { ...state, isEditItemLoading: false, editItemError: action.editItemError }
    case CREDITDECISION_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case CREDITDECISION_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case CREDITDECISION_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
