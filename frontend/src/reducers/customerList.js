import {
  CUSTOMERLIST_FETCHITEMS_LOAD,
  CUSTOMERLIST_FETCHITEMS_SUCCEED,
  CUSTOMERLIST_FETCHITEMS_FAIL,
  CUSTOMERLIST_CHANGETABLE_CHANGE,
  CUSTOMERLIST_SEARCHTABLE_EDIT,
  CUSTOMERLIST_SEARCHTABLE_SEARCH,
  CUSTOMERFORM_DELETEITEM_LOAD,
  CUSTOMERFORM_DELETEITEM_SUCCEED,
  CUSTOMERFORM_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case CUSTOMERLIST_FETCHITEMS_LOAD:
      return { ...state, isFetchItemsLoading: true, fetchItemsError: null }
    case CUSTOMERLIST_FETCHITEMS_SUCCEED:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: action.items,
        pagination: { ...state.pagination, total: action.total },
      }
    case CUSTOMERLIST_FETCHITEMS_FAIL:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: null,
        fetchItemsError: action.fetchItemsError,
      }
    case CUSTOMERLIST_CHANGETABLE_CHANGE: {
      return {
        ...state,
        pagination: action.pagination,
        filters: action.filters,
        sorter: action.sorter,
      }
    }
    case CUSTOMERLIST_SEARCHTABLE_EDIT:
      return {
        ...state,
        search: action.search,
      }
    case CUSTOMERLIST_SEARCHTABLE_SEARCH:
      return {
        ...state,
        isSearching: action.isSearching,
      }
    case CUSTOMERFORM_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case CUSTOMERFORM_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case CUSTOMERFORM_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
