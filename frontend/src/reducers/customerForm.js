import {
  CUSTOMERFORM_ENTER_RESET,
  CUSTOMERFORM_EDITFORM_CHANGE,
  CUSTOMERFORM_FETCHITEM_LOAD,
  CUSTOMERFORM_FETCHITEM_SUCCEED,
  CUSTOMERFORM_FETCHITEM_FAIL,
  CUSTOMERFORM_CREATEITEM_LOAD,
  CUSTOMERFORM_CREATEITEM_SUCCEED,
  CUSTOMERFORM_CREATEITEM_FAIL,
  CUSTOMERFORM_EDITITEM_LOAD,
  CUSTOMERFORM_EDITITEM_SUCCEED,
  CUSTOMERFORM_EDITITEM_FAIL,
  CUSTOMERFORM_DELETEITEM_LOAD,
  CUSTOMERFORM_DELETEITEM_SUCCEED,
  CUSTOMERFORM_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case CUSTOMERFORM_FETCHITEM_LOAD:
      return { ...state, isFetchItemLoading: true, fetchItemError: null }
    case CUSTOMERFORM_FETCHITEM_SUCCEED:
      return { ...state, isFetchItemLoading: false, item: action.item }
    case CUSTOMERFORM_FETCHITEM_FAIL:
      return { ...state, isFetchItemLoading: false, fetchItemError: action.fetchItemError }
    case CUSTOMERFORM_ENTER_RESET:
      return {}
    case CUSTOMERFORM_EDITFORM_CHANGE:
      return { ...state, formFieldValues: { ...state.formFieldValues, ...action.field } }
    case CUSTOMERFORM_CREATEITEM_LOAD:
      return {
        ...state,
        isCreateItemLoading: true,
        isCreateItemSuccess: false,
        createItemError: null,
      }
    case CUSTOMERFORM_CREATEITEM_SUCCEED:
      return { ...state, isCreateItemLoading: false, isCreateItemSuccess: true }
    case CUSTOMERFORM_CREATEITEM_FAIL:
      return { ...state, isCreateItemLoading: false, createItemError: action.createItemError }
    case CUSTOMERFORM_EDITITEM_LOAD:
      return {
        ...state,
        isEditItemLoading: true,
        isEditItemSuccess: false,
        editItemError: null,
      }
    case CUSTOMERFORM_EDITITEM_SUCCEED:
      return { ...state, isEditItemLoading: false, isEditItemSuccess: true }
    case CUSTOMERFORM_EDITITEM_FAIL:
      return { ...state, isEditItemLoading: false, editItemError: action.editItemError }
    case CUSTOMERFORM_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case CUSTOMERFORM_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case CUSTOMERFORM_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
