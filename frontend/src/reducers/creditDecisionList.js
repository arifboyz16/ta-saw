import {
  CREDITDECISION_FETCHITEMS_LOAD,
  CREDITDECISION_FETCHITEMS_SUCCEED,
  CREDITDECISION_FETCHITEMS_FAIL,
  CREDITDECISION_CHANGETABLE_CHANGE,
  CREDITDECISION_SEARCHTABLE_EDIT,
  CREDITDECISION_SEARCHTABLE_SEARCH,
  CREDITDECISION_DELETEITEM_LOAD,
  CREDITDECISION_DELETEITEM_SUCCEED,
  CREDITDECISION_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case CREDITDECISION_FETCHITEMS_LOAD:
      return { ...state, isFetchItemsLoading: true, fetchItemsError: null }
    case CREDITDECISION_FETCHITEMS_SUCCEED:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: action.items,
        pagination: { ...state.pagination, total: action.total },
      }
    case CREDITDECISION_FETCHITEMS_FAIL:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: null,
        fetchItemsError: action.fetchItemsError,
      }
    case CREDITDECISION_CHANGETABLE_CHANGE: {
      return {
        ...state,
        pagination: action.pagination,
        filters: action.filters,
        sorter: action.sorter,
      }
    }
    case CREDITDECISION_SEARCHTABLE_EDIT:
      return {
        ...state,
        search: action.search,
      }
    case CREDITDECISION_SEARCHTABLE_SEARCH:
      return {
        ...state,
        isSearching: action.isSearching,
      }
    case CREDITDECISION_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case CREDITDECISION_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case CREDITDECISION_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
