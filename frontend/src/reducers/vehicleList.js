import {
  VEHICLELIST_FETCHITEMS_LOAD,
  VEHICLELIST_FETCHITEMS_SUCCEED,
  VEHICLELIST_FETCHITEMS_FAIL,
  VEHICLELIST_CHANGETABLE_CHANGE,
  VEHICLELIST_SEARCHTABLE_EDIT,
  VEHICLELIST_SEARCHTABLE_SEARCH,
  VEHICLEFORM_DELETEITEM_LOAD,
  VEHICLEFORM_DELETEITEM_SUCCEED,
  VEHICLEFORM_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case VEHICLELIST_FETCHITEMS_LOAD:
      return { ...state, isFetchItemsLoading: true, fetchItemsError: null }
    case VEHICLELIST_FETCHITEMS_SUCCEED:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: action.items,
        pagination: { ...state.pagination, total: action.total },
      }
    case VEHICLELIST_FETCHITEMS_FAIL:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: null,
        fetchItemsError: action.fetchItemsError,
      }
    case VEHICLELIST_CHANGETABLE_CHANGE: {
      return {
        ...state,
        pagination: action.pagination,
        filters: action.filters,
        sorter: action.sorter,
      }
    }
    case VEHICLELIST_SEARCHTABLE_EDIT:
      return {
        ...state,
        search: action.search,
      }
    case VEHICLELIST_SEARCHTABLE_SEARCH:
      return {
        ...state,
        isSearching: action.isSearching,
      }
    case VEHICLEFORM_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case VEHICLEFORM_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case VEHICLEFORM_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
