import { combineReducers } from 'redux'
import app from './app'
import home from './home'
import login from './login'
import admin from './admin'
import userList from './userList'
import userForm from './userForm'
import vehicleList from './vehicleList'
import vehicleForm from './vehicleForm'
import leasingList from './leasingList'
import leasingForm from './leasingForm'
import customerList from './customerList'
import customerForm from './customerForm'
import creditDecisionList from './creditDecisionList'
import creditDecisionForm from './creditDecisionForm'

export default combineReducers({
  app,
  home,
  login,
  admin,
  userList,
  userForm,
  customerList,
  customerForm,
  vehicleList,
  vehicleForm,
  leasingList,
  leasingForm,
  creditDecisionList,
  creditDecisionForm
})
