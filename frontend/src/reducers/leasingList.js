import {
  LEASINGLIST_FETCHITEMS_LOAD,
  LEASINGLIST_FETCHITEMS_SUCCEED,
  LEASINGLIST_FETCHITEMS_FAIL,
  LEASINGLIST_CHANGETABLE_CHANGE,
  LEASINGLIST_SEARCHTABLE_EDIT,
  LEASINGLIST_SEARCHTABLE_SEARCH,
  LEASINGFORM_DELETEITEM_LOAD,
  LEASINGFORM_DELETEITEM_SUCCEED,
  LEASINGFORM_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case LEASINGLIST_FETCHITEMS_LOAD:
      return { ...state, isFetchItemsLoading: true, fetchItemsError: null }
    case LEASINGLIST_FETCHITEMS_SUCCEED:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: action.items,
        pagination: { ...state.pagination, total: action.total },
      }
    case LEASINGLIST_FETCHITEMS_FAIL:
      return {
        ...state,
        isFetchItemsLoading: false,
        items: null,
        fetchItemsError: action.fetchItemsError,
      }
    case LEASINGLIST_CHANGETABLE_CHANGE: {
      return {
        ...state,
        pagination: action.pagination,
        filters: action.filters,
        sorter: action.sorter,
      }
    }
    case LEASINGLIST_SEARCHTABLE_EDIT:
      return {
        ...state,
        search: action.search,
      }
    case LEASINGLIST_SEARCHTABLE_SEARCH:
      return {
        ...state,
        isSearching: action.isSearching,
      }
    case LEASINGFORM_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case LEASINGFORM_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case LEASINGFORM_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
