import {
  LEASINGFORM_ENTER_RESET,
  LEASINGFORM_EDITFORM_CHANGE,
  LEASINGFORM_FETCHITEM_LOAD,
  LEASINGFORM_FETCHITEM_SUCCEED,
  LEASINGFORM_FETCHITEM_FAIL,
  LEASINGFORM_CREATEITEM_LOAD,
  LEASINGFORM_CREATEITEM_SUCCEED,
  LEASINGFORM_CREATEITEM_FAIL,
  LEASINGFORM_EDITITEM_LOAD,
  LEASINGFORM_EDITITEM_SUCCEED,
  LEASINGFORM_EDITITEM_FAIL,
  LEASINGFORM_DELETEITEM_LOAD,
  LEASINGFORM_DELETEITEM_SUCCEED,
  LEASINGFORM_DELETEITEM_FAIL
} from '@/constants/actionTypes'

export default (state = {}, action) => {
  switch (action.type) {
    case LEASINGFORM_FETCHITEM_LOAD:
      return { ...state, isFetchItemLoading: true, fetchItemError: null }
    case LEASINGFORM_FETCHITEM_SUCCEED:
      return { ...state, isFetchItemLoading: false, item: action.item }
    case LEASINGFORM_FETCHITEM_FAIL:
      return { ...state, isFetchItemLoading: false, fetchItemError: action.fetchItemError }
    case LEASINGFORM_ENTER_RESET:
      return {}
    case LEASINGFORM_EDITFORM_CHANGE:
      return { ...state, formFieldValues: { ...state.formFieldValues, ...action.field } }
    case LEASINGFORM_CREATEITEM_LOAD:
      return {
        ...state,
        isCreateItemLoading: true,
        isCreateItemSuccess: false,
        createItemError: null,
      }
    case LEASINGFORM_CREATEITEM_SUCCEED:
      return { ...state, isCreateItemLoading: false, isCreateItemSuccess: true }
    case LEASINGFORM_CREATEITEM_FAIL:
      return { ...state, isCreateItemLoading: false, createItemError: action.createItemError }
    case LEASINGFORM_EDITITEM_LOAD:
      return {
        ...state,
        isEditItemLoading: true,
        isEditItemSuccess: false,
        editItemError: null,
      }
    case LEASINGFORM_EDITITEM_SUCCEED:
      return { ...state, isEditItemLoading: false, isEditItemSuccess: true }
    case LEASINGFORM_EDITITEM_FAIL:
      return { ...state, isEditItemLoading: false, editItemError: action.editItemError }
    case LEASINGFORM_DELETEITEM_LOAD:
      return {
        ...state,
        isDeleteItemLoading: true,
        isDeleteItemSuccess: false,
        deleteItemError: null,
      }
    case LEASINGFORM_DELETEITEM_SUCCEED:
      return { ...state, isDeleteItemLoading: false, isDeleteItemSuccess: true }
    case LEASINGFORM_DELETEITEM_FAIL:
      return { ...state, isDeleteItemLoading: false, deleteItemError: action.deleteItemError }
    default:
      return state
  }
}
