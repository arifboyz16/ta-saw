import axios from 'axios';
import { API } from './urls';
import { history } from '@/store';

const method = API.SECURE ? 'https' : 'http'
let url
if (API.PORT !== null){
	url = `${method}://${API.URL}:${API.PORT}`
}else{
	url = `${method}://${API.URL}`
}

const codeMessage = {
  200: 'The server successfully returned the requested data. ',
  201: 'New or modified data is successful. ',
  202: 'A request has entered the background queue (asynchronous task). ',
  204: 'Delete data successfully. ',
  400: 'The request was sent with an error. The server did not perform any operations to create or modify data. ',
  401: 'The user does not have permission (token, username, password is incorrect). ',
  403: 'User is authorized, but access is forbidden. ',
  404: 'The request was made to a record that does not exist, and the server did not operate. ',
  406: 'The format of the request is not available. ',
  410: 'The requested resource is permanently deleted and will not be obtained again. ',
  422: 'When creating an object, a validation error occurred. ',
  500: 'The server has an error, please check the server. ',
  502: 'Gateway error. ',
  503: 'The service is unavailable, the server is temporarily overloaded or maintained. ',
  504: 'The gateway timed out. ',
};


const http = axios.create ({
  baseURL: url,
  timeout: 1000,
  headers: {
      'Content-Type': 'application/json',
  },
  validateStatus: function (status) {
     if (status >= 200 && status < 300) {
      return status;
    }
    const errortext = codeMessage[status] 
    if (status === 403) {
      // history.push('/403')
      localStorage.removeItem('accessToken')
      localStorage.removeItem('user')
      return;
    }
    if (status === 401) {
      // history.push('/403')
      return status;
    }
    if (status >= 404 && status < 422) {
      // history.push('/404');
      return;
    }
    if (status <= 504 && status >= 500) {
      // history.push('/500');
      return;
    }
  },
});

export async function request(
  url,
  method,
  data = null
){
  let response
  let token = localStorage.getItem('accessToken')
  if (token) {
    http.defaults.headers.common = {'Authorization': "Bearer " + token}
  }
  if(method === 'POST'){
    response = await http.post(url,  data)
  }
  else if(method === 'GET'){
    response = await http.get(url, { params: data })
  }
  else if(method === 'PUT'){
    response = await http.put(url,  data)
  }
  else if(method === 'DELETE'){
    response = await http.delete(url)
  }
  return response
}
