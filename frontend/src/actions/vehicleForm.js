import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request'

export const editForm = formFieldsChange => ({
  type: ActionTypes.VEHICLEFORM_EDITFORM_CHANGE,
  field: formFieldsChange,
})

export const reset = () => ({
  type: ActionTypes.VEHICLEFORM_ENTER_RESET,
})

export const fetchItem = id => (dispatch) => {
  dispatch({ type: ActionTypes.VEHICLEFORM_FETCHITEM_LOAD })
  const { id: id_items } = id
  return request('/api/vehicle/'+id_items, 'GET')
  .then(data => {
    const { data: item } = data
    dispatch({
      type: ActionTypes.VEHICLEFORM_FETCHITEM_SUCCEED,
      item,
    })
    dispatch({
      type: ActionTypes.VEHICLEFORM_EDITFORM_CHANGE,
      field: {
        id: { name: 'id', value: item.id },
        vehicle_code: { name: 'vehicle_code', value: item.vehicle_code },
        vehicle_name: { name: 'vehicle_name', value: item.vehicle_name },
        vehicle_type: { name: 'vehicle_type', value: item.vehicle_type },
        vehicle_price: { name: 'vehicle_price', value: item.vehicle_price },
      },
    })
  }).catch(err => {
    dispatch({
      type: ActionTypes.VEHICLEFORM_FETCHITEM_FAIL,
      fetchItemError: 'This is a fetch item error.',
    })
  })
}

export const createItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.VEHICLEFORM_CREATEITEM_LOAD })
  const data = values
  return request('/api/vehicle/', 'POST', data)
  .then(response => {
    // dispatch({ type: ActionTypes.VEHICLEFORM_CHANGETABLE_CHANGE })
    dispatch({ type: ActionTypes.VEHICLEFORM_CREATEITEM_SUCCEED })
    dispatch({ type: ActionTypes.VEHICLEFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.VEHICLEFORM_CREATEITEM_FAIL,
      createItemError: 'This is a create item error.',
    })
  })
}

export const editItem = params => (dispatch) => {
  dispatch({ type: ActionTypes.VEHICLEFORM_EDITITEM_LOAD })
  const data = params
  return request('/api/vehicle/'+params.id, 'PUT', data)
  .then(response => {
    dispatch({ type: ActionTypes.VEHICLEFORM_EDITITEM_SUCCEED })
  }).catch(err =>{
     dispatch({
      type: ActionTypes.VEHICLEFORM_EDITITEM_FAIL,
      editItemError: 'This is a edit item error.',
    })
  })
}

export const deleteItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.VEHICLEFORM_DELETEITEM_LOAD })
  return request('/api/vehicle/'+values.id, 'DELETE')
  .then(response => {
    dispatch({ type: ActionTypes.VEHICLEFORM_DELETEITEM_SUCCEED })
    dispatch({ type: ActionTypes.VEHICLEFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.VEHICLEFORM_DELETEITEM_FAIL,
      deleteItemError: 'This is a delete item error.',
    })
  })
}
