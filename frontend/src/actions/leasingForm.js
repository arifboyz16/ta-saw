import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request'

export const editForm = formFieldsChange => ({
  type: ActionTypes.LEASINGFORM_EDITFORM_CHANGE,
  field: formFieldsChange,
})

export const reset = () => ({
  type: ActionTypes.LEASINGFORM_ENTER_RESET,
})

export const fetchItem = id => (dispatch) => {
  dispatch({ type: ActionTypes.LEASINGFORM_FETCHITEM_LOAD })
  const { id: id_items } = id
  return request('/api/leasing/'+id_items, 'GET')
  .then(data => {
    const { data: item } = data
    dispatch({
      type: ActionTypes.LEASINGFORM_FETCHITEM_SUCCEED,
      item,
    })
    dispatch({
      type: ActionTypes.LEASINGFORM_EDITFORM_CHANGE,
      field: {
        id: { name: 'id', value: item.id },
        leasing_code: { name: 'leasing_code', value: item.leasing_code },
        leasing_name: { name: 'leasing_name', value: item.leasing_name },
        leasing_rate: { name: 'leasing_rate', value: item.leasing_rate },
      },
    })
  }).catch(err => {
    dispatch({
      type: ActionTypes.LEASINGFORM_FETCHITEM_FAIL,
      fetchItemError: 'This is a fetch item error.',
    })
  })
}

export const createItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.LEASINGFORM_CREATEITEM_LOAD })
  const data = values
  return request('/api/leasing/', 'POST', data)
  .then(response => {
    // dispatch({ type: ActionTypes.LEASINGFORM_CHANGETABLE_CHANGE })
    dispatch({ type: ActionTypes.LEASINGFORM_CREATEITEM_SUCCEED })
    dispatch({ type: ActionTypes.LEASINGFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.LEASINGFORM_CREATEITEM_FAIL,
      createItemError: 'This is a create item error.',
    })
  })
}

export const editItem = params => (dispatch) => {
  dispatch({ type: ActionTypes.LEASINGFORM_EDITITEM_LOAD })
  const data = params
  return request('/api/leasing/'+params.id, 'PUT', data)
  .then(response => {
    dispatch({ type: ActionTypes.LEASINGFORM_EDITITEM_SUCCEED })
  }).catch(err =>{
     dispatch({
      type: ActionTypes.LEASINGFORM_EDITITEM_FAIL,
      editItemError: 'This is a edit item error.',
    })
  })
}

export const deleteItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.LEASINGFORM_DELETEITEM_LOAD })
  return request('/api/leasing/'+values.id, 'DELETE')
  .then(response => {
    dispatch({ type: ActionTypes.LEASINGFORM_DELETEITEM_SUCCEED })
    dispatch({ type: ActionTypes.LEASINGFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.LEASINGFORM_DELETEITEM_FAIL,
      deleteItemError: 'This is a delete item error.',
    })
  })
}
