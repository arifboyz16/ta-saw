import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request'

export const editForm = formFieldsChange => ({
  type: ActionTypes.USERFORM_EDITFORM_CHANGE,
  field: formFieldsChange,
})

export const reset = () => ({
  type: ActionTypes.USERFORM_ENTER_RESET,
})

export const fetchItem = id => (dispatch) => {
  dispatch({ type: ActionTypes.USERFORM_FETCHITEM_LOAD })
  const { id: id_items } = id
  return request('/api/user/'+id_items, 'GET')
  .then(data => {
    const { data: item } = data
    dispatch({
      type: ActionTypes.USERFORM_FETCHITEM_SUCCEED,
      item,
    })
    dispatch({
      type: ActionTypes.USERFORM_EDITFORM_CHANGE,
      field: {
        id: { name: 'id', value: item.id },
        nrp: { name: 'nrp', value: item.nrp },
        email: { name: 'email', value: item.email },
        username: { name: 'username', value: item.username },
        first_name: { name: 'first_name', value: item.first_name },
        last_name: { name: 'last_name', value: item.last_name },
        password: { name: 'password', value: item.password },
        handphone: { name: 'handphone', value: item.handphone },
        posisi: { name: 'posisi', value: item.posisi },
      },
    })
  }).catch(err => {
    dispatch({
      type: ActionTypes.USERFORM_FETCHITEM_FAIL,
      fetchItemError: 'This is a fetch item error.',
    })
  })
}

export const createItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.USERFORM_CREATEITEM_LOAD })
  const data = values
  return request('/api/user/', 'POST', data)
  .then(response => {
    // dispatch({ type: ActionTypes.USERFORM_CHANGETABLE_CHANGE })
    dispatch({ type: ActionTypes.USERFORM_CREATEITEM_SUCCEED })
    dispatch({ type: ActionTypes.USERFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.USERFORM_CREATEITEM_FAIL,
      createItemError: 'This is a create item error.',
    })
  })
}

export const editItem = params => (dispatch) => {
  dispatch({ type: ActionTypes.USERFORM_EDITITEM_LOAD })
  const data = params
  return request('/api/user/'+params.id, 'PUT', data)
  .then(response => {
    dispatch({ type: ActionTypes.USERFORM_EDITITEM_SUCCEED })
  }).catch(err =>{
     dispatch({
      type: ActionTypes.USERFORM_EDITITEM_FAIL,
      editItemError: 'This is a edit item error.',
    })
  })
}

export const deleteItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.USERFORM_DELETEITEM_LOAD })
  return request('/api/user/'+values.id, 'DELETE')
  .then(response => {
    dispatch({ type: ActionTypes.USERFORM_DELETEITEM_SUCCEED })
    dispatch({ type: ActionTypes.USERFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.USERFORM_DELETEITEM_FAIL,
      deleteItemError: 'This is a delete item error.',
    })
  })
}
