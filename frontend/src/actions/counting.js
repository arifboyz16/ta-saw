import { request } from '@/utils/request';

export const creditSimulation = (values) => {
	return request('api/perhitungan/credit-simulation',  'POST', values)
    .then(data => {
        return data;
    }).catch(err => {
    	return err
    })
}

export const sawCalculation = (values) => {
    return request('api/perhitungan/saw-simulation',  'POST', values)
    .then(data => {
        return data;
    }).catch(err => {
        return err
    })
}