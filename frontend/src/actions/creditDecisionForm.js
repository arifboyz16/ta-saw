import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request'

export const editForm = formFieldsChange => ({
  type: ActionTypes.CREDITDECISION_EDITFORM_CHANGE,
  field: formFieldsChange,
})

export const reset = () => ({
  type: ActionTypes.CREDITDECISION_ENTER_RESET,
})

export const fetchItem = id => (dispatch) => {
  dispatch({ type: ActionTypes.CREDITDECISION_FETCHITEM_LOAD })
  const { id: id_items } = id
  return request('/api/credit-decision/'+id_items, 'GET')
  .then(data => {
    const { data: item } = data
    dispatch({
      type: ActionTypes.CREDITDECISION_FETCHITEM_SUCCEED,
      item,
    })
    dispatch({
      type: ActionTypes.CREDITDECISION_EDITFORM_CHANGE,
      field: {
        id: { name: 'id', value: item.id },
        customer_id: { name: 'nik', value: item.customer_id },
        name: { name: 'name', value: item.name },
        address: { name: 'address', value: item.address },
        handphone: { name: 'handphone', value: item.handphone },
        earning: { name: 'earning', value: item.earning },
        house_status: { name: 'house_status', value: item.house_status },
        other_income: { name: 'other_income', value: item.other_income },
        house_garage: { name: 'house_garage', value: item.house_garage },
        other_payment: { name: 'other_payment', value: item.other_payment },
        credit_history: { name: 'credit_history', value: item.credit_history },
        down_payment: { name: 'down_payment', value: item.down_payment },
        periode: { name: 'periode', value: item.periode },
        decision: { name: 'decision', value: item.decision },
      },
    })
  }).catch(err => {
    dispatch({
      type: ActionTypes.CREDITDECISION_FETCHITEM_FAIL,
      fetchItemError: 'This is a fetch item error.',
    })
  })
}

export const createItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.CREDITDECISION_CREATEITEM_LOAD })
  const data = values
  return request('/api/credit-decision', 'POST', data)
  .then(response => {
    // dispatch({ type: ActionTypes.CREDITDECISION_CHANGETABLE_CHANGE })
    dispatch({ type: ActionTypes.CREDITDECISION_CREATEITEM_SUCCEED })
    dispatch({ type: ActionTypes.CREDITDECISION_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.CREDITDECISION_CREATEITEM_FAIL,
      createItemError: 'This is a create item error.',
    })
  })
}

export const editItem = params => (dispatch) => {
  dispatch({ type: ActionTypes.CREDITDECISION_EDITITEM_LOAD })
  const data = params
  return request('/api/credit-decision/'+params.id, 'PUT', data)
  .then(response => {
    dispatch({ type: ActionTypes.CREDITDECISION_EDITITEM_SUCCEED })
  }).catch(err =>{
     dispatch({
      type: ActionTypes.CREDITDECISION_EDITITEM_FAIL,
      editItemError: 'This is a edit item error.',
    })
  })
}

export const deleteItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.CREDITDECISION_DELETEITEM_LOAD })
  return request('/api/vehicle/'+values.id, 'DELETE')
  .then(response => {
    dispatch({ type: ActionTypes.CREDITDECISION_DELETEITEM_SUCCEED })
    dispatch({ type: ActionTypes.CREDITDECISION_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.CREDITDECISION_DELETEITEM_FAIL,
      deleteItemError: 'This is a delete item error.',
    })
  })
}
