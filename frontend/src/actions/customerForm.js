import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request'

export const editForm = formFieldsChange => ({
  type: ActionTypes.CUSTOMERFORM_EDITFORM_CHANGE,
  field: formFieldsChange,
})

export const reset = () => ({
  type: ActionTypes.CUSTOMERFORM_ENTER_RESET,
})

export const fetchItem = id => (dispatch) => {
  dispatch({ type: ActionTypes.CUSTOMERFORM_FETCHITEM_LOAD })
  const { id: id_items } = id
  return request('/api/customer/'+id_items, 'GET')
  .then(data => {
    const { data: item } = data
    dispatch({
      type: ActionTypes.CUSTOMERFORM_FETCHITEM_SUCCEED,
      item,
    })
    dispatch({
      type: ActionTypes.CUSTOMERFORM_EDITFORM_CHANGE,
      field: {
        id: { name: 'id', value: item.id },
        nik: { name: 'nik', value: item.nik },
        name: { name: 'name', value: item.name },
        address: { name: 'address', value: item.address },
        handphone: { name: 'handphone', value: item.handphone },
        earning: { name: 'earning', value: item.earning },
        tax_payer_id: { name: 'tax_payer_id', value: item.tax_payer_id },
      },
    })
  }).catch(err => {
    dispatch({
      type: ActionTypes.CUSTOMERFORM_FETCHITEM_FAIL,
      fetchItemError: 'This is a fetch item error.',
    })
  })
}

export const createItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.CUSTOMERFORM_CREATEITEM_LOAD })
  const data = values
  return request('/api/customer/', 'POST', data)
  .then(response => {
    // dispatch({ type: ActionTypes.CUSTOMERFORM_CHANGETABLE_CHANGE })
    dispatch({ type: ActionTypes.CUSTOMERFORM_CREATEITEM_SUCCEED })
    dispatch({ type: ActionTypes.CUSTOMERFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.CUSTOMERFORM_CREATEITEM_FAIL,
      createItemError: 'This is a create item error.',
    })
  })
}

export const editItem = params => (dispatch) => {
  dispatch({ type: ActionTypes.CUSTOMERFORM_EDITITEM_LOAD })
  const data = params
  return request('/api/customer/'+params.id, 'PUT', data)
  .then(response => {
    dispatch({ type: ActionTypes.CUSTOMERFORM_EDITITEM_SUCCEED })
  }).catch(err =>{
     dispatch({
      type: ActionTypes.CUSTOMERFORM_EDITITEM_FAIL,
      editItemError: 'This is a edit item error.',
    })
  })
}

export const deleteItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.CUSTOMERFORM_DELETEITEM_LOAD })
  return request('/api/customer/'+values.id, 'DELETE')
  .then(response => {
    dispatch({ type: ActionTypes.CUSTOMERFORM_DELETEITEM_SUCCEED })
    dispatch({ type: ActionTypes.CUSTOMERFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.CUSTOMERFORM_DELETEITEM_FAIL,
      deleteItemError: 'This is a delete item error.',
    })
  })
}
