import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request'

export const fetchItems = (params = {}) => async (dispatch) => {
  dispatch({ type: ActionTypes.LEASINGLIST_FETCHITEMS_LOAD })
  const {
    current = 1,
    pageSize = 10,
    sort,
    order = 'descend',
    search,
  } = params
  try {
      const response = await request('/api/leasing/',  'GET', params)
      const data = await response.data;
      const { results, total } = data
      const $skip = (current - 1) * pageSize
      const $limit = pageSize
      dispatch({
        type: ActionTypes.LEASINGLIST_FETCHITEMS_SUCCEED,
        items: results,
        total,
      })
  } catch (e) {
    dispatch({
      type: ActionTypes.LEASINGLIST_FETCHITEMS_FAIL,
      fetchItemsError: 'This is a error',
    })
  }
}

export const changeTable = params => (dispatch) => {
  const { pagination = {}, filters = {}, sorter = {} } = params
  dispatch({
    type: ActionTypes.LEASINGLIST_CHANGETABLE_CHANGE,
    pagination,
    filters,
    sorter,
  })
}

export const searchTable = ({ isSearching }) => (dispatch) => {
  dispatch({
    type: ActionTypes.LEASINGLIST_SEARCHTABLE_SEARCH,
    isSearching,
  })
}

export const editSearch = params => (dispatch) => {
  const { search } = params
  dispatch({
    type: ActionTypes.LEASINGLIST_SEARCHTABLE_EDIT,
    search,
  })
}

export const deleteItem = values => (dispatch) => {
  dispatch({ type: ActionTypes.VEHICLEFORM_DELETEITEM_LOAD })
  console.log(values)
  return request('/api/leasing/'+values.id, 'DELETE')
  .then(response => {
    dispatch({ type: ActionTypes.VEHICLEFORM_DELETEITEM_SUCCEED })
    dispatch({ type: ActionTypes.VEHICLEFORM_ENTER_RESET })
  }).catch(err =>{
    dispatch({
      type: ActionTypes.VEHICLEFORM_DELETEITEM_FAIL,
      deleteItemError: 'This is a delete item error.',
    })
  })
}
