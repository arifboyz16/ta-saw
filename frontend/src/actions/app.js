import * as ActionTypes from '@/constants/actionTypes'
import { request } from '@/utils/request';

export const login = (values) => (dispatch) => {
	const data = values.values
	return request('/api/auth/login/',  'POST', data)
    .then(user => {
        if (user.status === 401) {
          dispatch({ type: ActionTypes.LOGIN_LOGIN_FAIL, loginError: 'Wrong email / password.' })
        } else if (user.data !== undefined) {
          localStorage.setItem('accessToken', user.data.token)
          localStorage.setItem('user', JSON.stringify(user.data.user))
          dispatch({ type: ActionTypes.APP_LOGIN_SET, accessToken: user.data.token })
            dispatch({ type: ActionTypes.LOGIN_LOGIN_SUCCEED })
        } 
        return user;
    }).catch(err => {
    	dispatch({ type: ActionTypes.LOGIN_LOGIN_FAIL, loginError: 'Wrong email / password.' })
    })
}

export const logout = () => (dispatch) => {
  localStorage.removeItem('accessToken')
  localStorage.removeItem('user')
  dispatch({ type: ActionTypes.APP_LOGOUT_SET })
}
