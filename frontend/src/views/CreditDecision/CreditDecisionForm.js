import React from 'react'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router-dom'
import { message } from 'antd'
import AdminLayout from '@/components/AdminLayout'
import SectionHeader from '@/components/SectionHeader'
import SectionHeaderTemplate from '@/components/SectionHeaderTemplate'
import SectionContent from '@/components/SectionContent'
import Spin from '@/components/Spin'
import { FormCreditDecision } from '@/components/CreditDecisionComp'
import { sawCalculation } from '@/actions/counting'
import {
  editForm,
  fetchItem,
  createItem,
  editItem,
  deleteItem,
  reset,
} from '@/actions/creditDecisionForm'
import {
  fetchItems as getCustomerList,
  reset as resetLeasing,
} from '@/actions/customerList'

import {
  fetchItem as getDetailCustomer,
  reset as resetCustomer,
} from '@/actions/customerForm'

const listPath = '/admin/credit-decision'
const itemsTitle = 'Credit Decision'
const itemTitle = 'Credit Decision'
const storeKey = 'creditDecisionForm'

class CreditDecisionForm extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      decision: {}
    }
    this.handleFormOnSubmit = this.handleFormOnSubmit.bind(this)
    this.handleFormOnDelete = this.handleFormOnDelete.bind(this)
    this.handleGetCustomerDetail = this.handleGetCustomerDetail.bind(this)
    this.handleGetCustomerSearch = this.handleGetCustomerSearch.bind(this)
    this.handleBackButtonOnClick = this.handleBackButtonOnClick.bind(this)
    this.onProcessSAW = this.onProcessSAW.bind(this)
    this.props.reset()
    this.props.resetCustomer()
  }

  componentDidMount() {
    this.getUser()
    if (this.props.type === 'edit') {
      this.props.fetchItem({ id: this.props.match.params.itemId })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isCreateItemLoading && nextProps.isCreateItemSuccess) {
      message.success('You have successfully created the item.')
    } else if (this.props.isEditItemLoading && nextProps.isEditItemSuccess) {
      message.success('You have successfully edited the item.')
    } else if (this.props.isDeleteItemLoading && nextProps.isDeleteItemSuccess) {
      message.success('You have successfully deleted the item.')
    } else if (this.props.isCreateItemLoading && nextProps.createItemError) {
      message.error(nextProps.createItemError)
    } else if (this.props.isEditItemLoading && nextProps.editItemError) {
      message.error(nextProps.editItemError)
    } else if (this.props.isDeleteItemLoading && nextProps.deleteItemError) {
      message.error(nextProps.deleteItemError)
    } else if (this.props.isFetchItemLoading && nextProps.fetchItemError) {
      message.error(nextProps.fetchItemError)
    }
  }

  handleFormOnSubmit(values) {
    if (this.props.type === 'create') {
      this.props.createItem(values)
    } else {
      const item = { ...this.props.item, ...values }
      this.props.editItem(item)
    }
  }

  handleBackButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(listPath)
  }

  handleFormOnDelete() {
    this.props.deleteItem({ id: this.props.match.params.itemId })
  }

  getUser() {
    const {
      pagination = {},
      sorter = {},
      search,
      isSearching,
    } = this.props
    this.props.getCustomerList({
      current: pagination.current,
      pageSize: pagination.pageSize,
      sort: sorter.columnKey,
      order: sorter.order,
      search: (isSearching && search) || null,
    })
  }

  async onProcessSAW(data) {
    const response = await sawCalculation(data)
    const calculate = { decision: { name: 'decision', value: response.data[0] }}
    this.setState({ decision: calculate })
  }

  handleGetCustomerDetail(id) {
    this.props.getDetailCustomer({ id: id })
  }

  async handleGetCustomerSearch(search) {
    const {
      pagination = {},
      sorter = {},
      isSearching,
    } = this.props
    this.props.getCustomerList({
      current: pagination.current,
      pageSize: pagination.pageSize,
      sort: sorter.columnKey,
      order: sorter.order,
      search: search,
    })
  }

  render() {
    const { type, isCreateItemSuccess, isEditItemSuccess, customerList, customerDetail, formFieldValues, isDeleteItemSuccess } = this.props
    const listCustomer = customerList ? customerList : []
    const isCreateForm = type === 'create'
    const actionTitle = isCreateForm ? 'Create' : 'Edit'
    if (isCreateItemSuccess || isEditItemSuccess || isDeleteItemSuccess) {
      return <Redirect to={listPath} />
    }
    const formField = {
      ...customerDetail,
      ...formFieldValues,
      ...this.state.decision
    }
    return (
      <div>
        <AdminLayout>
          <SectionHeader>
            <SectionHeaderTemplate
              breadcrumbRoutes={[{ path: '/admin', title: 'Home' }, { path: listPath, title: itemsTitle }, { title: actionTitle }]}
              title={`${actionTitle} ${itemTitle}`}
            />
          </SectionHeader>
          <SectionContent>
            {
              this.props.isFetchItemLoading && <Spin />
            }
            {
              (isCreateForm || (!isCreateForm && this.props.item)) &&
              <FormCreditDecision
                BackButtonOnClick={this.handleBackButtonOnClick}
                onSubmit={this.handleFormOnSubmit}
                onDelete={this.handleFormOnDelete}
                onGetCustomerDetail={this.handleGetCustomerDetail}
                onGetCustomerSearch={this.handleGetCustomerSearch}
                onFieldsChange={this.props.editForm}
                listCustomer={listCustomer}
                formFieldValues={formField}
                isCreateItemLoading={this.props.isCreateItemLoading}
                isEditItemLoading={this.props.isEditItemLoading}
                isDeleteItemLoading={this.props.isDeleteItemLoading}
                type={this.props.type}
                onSAW={this.onProcessSAW}
              />
            }
          </SectionContent>
        </AdminLayout>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const {
    formFieldValues,
    isFetchItemLoading,
    isEditItemLoading,
    isCreateItemLoading,
    isCreateItemSuccess,
    isEditItemSuccess,
    isDeleteItemLoading,
    isDeleteItemSuccess,
    item,
    fetchItemError,
    createItemError,
    editItemError,
    deleteItemError,
  } = state[storeKey]

  const { items: customerList  } = state['customerList']
  const { formFieldValues: customerDetail  } = state['customerForm']

  return {
    isFetchItemLoading,
    formFieldValues,
    isEditItemLoading,
    isCreateItemLoading,
    isCreateItemSuccess,
    isEditItemSuccess,
    isDeleteItemLoading,
    isDeleteItemSuccess,
    item,
    customerList,
    customerDetail,
    fetchItemError,
    createItemError,
    editItemError,
    deleteItemError,
  }
}

const mapDispatchToProps = dispatch => ({
  createItem: params => dispatch(createItem(params)),
  editItem: params => dispatch(editItem(params)),
  fetchItem: params => dispatch(fetchItem(params)),
  deleteItem: params => dispatch(deleteItem(params)),
  editForm: formFieldsChange => dispatch(editForm(formFieldsChange)),
  getCustomerList: params => dispatch(getCustomerList(params)),
  getDetailCustomer: params => dispatch(getDetailCustomer(params)),
  resetCustomer: () => dispatch(resetCustomer()),
  reset: () => dispatch(reset()),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreditDecisionForm))
