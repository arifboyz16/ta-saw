import React from 'react'
import { connect } from 'react-redux'
import { Card, Button } from 'antd'
import AdminLayout from '@/components/AdminLayout'
import SectionContent from '@/components/SectionContent'
import { MenuAdmin, MenuSales } from '@/components/Menu'

const userList = '/admin/users'
const customerList = '/admin/customer'
const vehicleList = '/admin/vehicle'
const leasingList = '/admin/leasing'
const creditSimulation = '/admin/credit-simulation'
const creditDecision = '/admin/credit-decision'

class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleUserButtonOnClick = this.handleUserButtonOnClick.bind(this)
    this.handleCustomerButtonOnClick = this.handleCustomerButtonOnClick.bind(this)
    this.handleVehicleButtonOnClick = this.handleVehicleButtonOnClick.bind(this)
    this.handleLeasingButtonOnClick = this.handleLeasingButtonOnClick.bind(this)
    this.handleCreditSimulationButtonOnClick = this.handleCreditSimulationButtonOnClick.bind(this)
    this.handleCreditDecisionButtonOnClick = this.handleCreditDecisionButtonOnClick.bind(this)
  }

  handleUserButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(userList)
  }

  handleCustomerButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(customerList)
  }

  handleVehicleButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(vehicleList)
  }

  handleLeasingButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(leasingList)
  }

  handleCreditSimulationButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(creditSimulation)
  }

  handleCreditDecisionButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(creditDecision)
  }

  render() {
	const user = JSON.parse(localStorage.getItem('user'))
	console.log(user)
    return (
      <AdminLayout>
		{user.role === 'sales' ? (
			<MenuSales handleCreditSimulation={this.handleCreditSimulationButtonOnClick} />
		) : (
			<MenuAdmin 
				handleUser={this.handleUserButtonOnClick}
				handleCustomer={this.handleCustomerButtonOnClick}
				handleCreditDecision={this.handleCreditDecisionButtonOnClick}
				handleCreditSimulation={this.handleCreditSimulationButtonOnClick}
				handleVehicle={this.handleVehicleButtonOnClick}
				handleLeasing={this.handleLeasingButtonOnClick}
			/>
		)}

      </AdminLayout>
    )
  }
}

export default connect()(Dashboard)

