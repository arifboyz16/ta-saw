import React from 'react'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import Login from '@/views/Login'
import NotFound from '@/views/NotFound'
import Dashboard from '@/views/Dashboard'
import UserList from '@/views/User/UserList'
import UserForm from '@/views/User/UserForm'
import CustomerList from '@/views/Customer/CustomerList'
import CustomerForm from '@/views/Customer/CustomerForm'
import VehicleList from '@/views/Vehicle/VehicleList'
import VehicleForm from '@/views/Vehicle/VehicleForm'
import LeasingList from '@/views/Leasing/LeasingList'
import LeasingForm from '@/views/Leasing/LeasingForm'
import CreditSimulationForm from '@/views/CreditSimulation/CreditSimulationForm'
import CreditDecisionForm from '@/views/CreditDecision/CreditDecisionForm'
import CreditDecisionList from '@/views/CreditDecision/CreditDecisionList'

import PrivateRoute from '@/components/PrivateRoute'
import './index.less'

export default class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={() => <Redirect to='/login' />} />
          <Route exact path='/login' component={Login} />
          <PrivateRoute exact path='/admin' component={() => <Redirect to='/admin/dashboard' />} />
          <PrivateRoute exact path='/admin/dashboard' component={Dashboard} />
          <PrivateRoute exact path='/admin/users' component={UserList} />
          <PrivateRoute exact path='/admin/users/create' component={props => <UserForm {...props} type='create' />} />
          <PrivateRoute exact path='/admin/users/edit/:itemId' component={props => <UserForm {...props} type='edit' />} />
          <PrivateRoute exact path='/admin/customer' component={CustomerList} />
          <PrivateRoute exact path='/admin/customer/create' component={props => <CustomerForm {...props} type='create' />} />
          <PrivateRoute exact path='/admin/customer/edit/:itemId' component={props => <CustomerForm {...props} type='edit' />} />
          <PrivateRoute exact path='/admin/vehicle' component={VehicleList} />
          <PrivateRoute exact path='/admin/vehicle/create' component={props => <VehicleForm {...props} type='create' />} />
          <PrivateRoute exact path='/admin/vehicle/edit/:itemId' component={props => <VehicleForm {...props} type='edit' />} />
          <PrivateRoute exact path='/admin/leasing' component={LeasingList} />
          <PrivateRoute exact path='/admin/leasing/create' component={props => <LeasingForm {...props} type='create' />} />
          <PrivateRoute exact path='/admin/leasing/edit/:itemId' component={props => <LeasingForm {...props} type='edit' />} />
          <PrivateRoute exact path='/admin/credit-simulation' component={props => <CreditSimulationForm {...props} type='create' />} />

          <PrivateRoute exact path='/admin/credit-decision' component={CreditDecisionList} />
          <PrivateRoute exact path='/admin/credit-decision/create' component={props => <CreditDecisionForm {...props} type='create' />} />
          <PrivateRoute exact path='/admin/credit-decision/edit/:itemId' component={props => <CreditDecisionForm {...props} type='edit' />} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    )
  }
}
