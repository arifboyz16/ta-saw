import React from 'react'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router-dom'
import { message } from 'antd'
import AdminLayout from '@/components/AdminLayout'
import SectionHeader from '@/components/SectionHeader'
import SectionHeaderTemplate from '@/components/SectionHeaderTemplate'
import SectionContent from '@/components/SectionContent'
import Spin from '@/components/Spin'
import { FormCreditSimulation } from '@/components/CreditSimulationComp'
import { creditSimulation } from '@/actions/counting'
import {
  fetchItems as getLeasingList,
} from '@/actions/leasingList'

import {
  fetchItems as getVehicleList,
} from '@/actions/vehicleList'

import {
  fetchItem as getDetailLeasing,
  reset as resetLeasing,
} from '@/actions/leasingForm'

import {
 fetchItem as getDetailVehicle,
 reset as resetVehicle,
} from '@/actions/vehicleForm'

import {
  editForm,
  fetchItem,
  createItem,
  editItem,
  deleteItem,
  reset,
} from '@/actions/userForm'

const listPath = '/admin/dashboard'
const itemsTitle = 'Credit Simulation'
const itemTitle = 'Credit Simulation'
const storeKey = 'userForm'

class CreditSimulationForm extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      installment: {}
    }
    this.handleFormOnSubmit = this.handleFormOnSubmit.bind(this)
    this.handleBackButtonOnClick = this.handleBackButtonOnClick.bind(this)
    this.handleBackButtonOnClick = this.handleBackButtonOnClick.bind(this)
    this.handleGetVehicleDetail = this.handleGetVehicleDetail.bind(this)
    this.handleGetLeasingDetail = this.handleGetLeasingDetail.bind(this)
    this.props.reset()
    this.props.resetLeasing()
    this.props.resetVehicle()
  }

  componentDidMount() {
    this.getLeasing()
    this.getVehicle()
    if (this.props.type === 'edit') {
      this.props.fetchItem({ id: this.props.match.params.itemId })
    }
  }

  getLeasing() {
    const {
      pagination = {},
      sorter = {},
      search,
      isSearching,
    } = this.props
    this.props.getLeasingList({
      current: pagination.current,
      pageSize: pagination.pageSize,
      sort: sorter.columnKey,
      order: sorter.order,
      search: (isSearching && search) || null,
    })
  }

  handleGetVehicleDetail(id) {
    this.props.getDetailVehicle({ id: id })
  }

  handleGetLeasingDetail(id) {
    this.props.getDetailLeasing({ id: id })
  }

  getVehicle() {
    const {
      pagination = {},
      sorter = {},
      search,
      isSearching,
    } = this.props
    this.props.getVehicleList({
      current: pagination.current,
      pageSize: pagination.pageSize,
      sort: sorter.columnKey,
      order: sorter.order,
      search: (isSearching && search) || null,
    })
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isCreateItemLoading && nextProps.isCreateItemSuccess) {
      message.success('You have successfully created the item.')
    } else if (this.props.isEditItemLoading && nextProps.isEditItemSuccess) {
      message.success('You have successfully edited the item.')
    } else if (this.props.isDeleteItemLoading && nextProps.isDeleteItemSuccess) {
      message.success('You have successfully deleted the item.')
    } else if (this.props.isCreateItemLoading && nextProps.createItemError) {
      message.error(nextProps.createItemError)
    } else if (this.props.isEditItemLoading && nextProps.editItemError) {
      message.error(nextProps.editItemError)
    } else if (this.props.isDeleteItemLoading && nextProps.deleteItemError) {
      message.error(nextProps.deleteItemError)
    } else if (this.props.isFetchItemLoading && nextProps.fetchItemError) {
      message.error(nextProps.fetchItemError)
    }
  }

  async handleFormOnSubmit(values) {
    const data = {
      harga_kendaraan: values.vehicle_price,
      leasing: values.leasing_rate,
      downpayment: values.down_payment,
      periode: values.period
    }
    const response = await creditSimulation(data)
    const calculate = { installment: { name: 'installment', value: response.data[0] }} 
    this.setState({ installment: calculate })
  }

  handleBackButtonOnClick(e) {
    e.preventDefault()
    this.props.history.push(listPath)
  }

  render() {
    const {
      type,
      isCreateItemSuccess,
      isDeleteItemSuccess,
      leasingList,
      vehicleList,
      vehicleDetail,
      leasingDetail,
      formFieldValues
    } = this.props

    const formField = {
      ...vehicleDetail,
      ...leasingDetail,
      ...formFieldValues,
      ...this.state.installment
    }

    const listLeasing = leasingList ? leasingList : [] 
    const listVehicle = vehicleList ? vehicleList : []

    const isCreateForm = type === 'create'
    const actionTitle = isCreateForm ? 'Create' : 'Edit'
    if (isCreateItemSuccess || isDeleteItemSuccess) {
      return <Redirect to={listPath} />
    }
    return (
      <div>
        <AdminLayout>
          <SectionHeader>
            <SectionHeaderTemplate
              breadcrumbRoutes={[{ path: '/admin', title: 'Home' }, { path: listPath, title: itemsTitle }, { title: actionTitle }]}
              title={`${actionTitle} ${itemTitle}`}
            />
          </SectionHeader>
          <SectionContent>
            {
              this.props.isFetchItemLoading && <Spin />
            }
            {
              (isCreateForm || (!isCreateForm && this.props.item)) &&
              <FormCreditSimulation
                BackButtonOnClick={this.handleBackButtonOnClick}
                onSubmit={this.handleFormOnSubmit}
                onGetVehicleDetail={this.handleGetVehicleDetail}
                onGetLeasingDetail={this.handleGetLeasingDetail}
                onFieldsChange={this.props.editForm}
                formFieldValues={formField}
                isCreateItemLoading={this.props.isCreateItemLoading}
                isEditItemLoading={this.props.isEditItemLoading}
                isDeleteItemLoading={this.props.isDeleteItemLoading}
                listLeasing={listLeasing}
                listVehicle={listVehicle}
                type={this.props.type}
              />
            }
          </SectionContent>
        </AdminLayout>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const {
    formFieldValues,
    isFetchItemLoading,
    isEditItemLoading,
    isCreateItemLoading,
    isCreateItemSuccess,
    isEditItemSuccess,
    isDeleteItemLoading,
    isDeleteItemSuccess,
    item,
    fetchItemError,
    createItemError,
    editItemError,
    deleteItemError,
  } = state[storeKey]

  const { items: vehicleList  } = state['vehicleList']
  const { items: leasingList  } = state['leasingList']

  const { formFieldValues: vehicleDetail  } = state['vehicleForm']
  const { formFieldValues: leasingDetail  } = state['leasingForm']

  return {
    isFetchItemLoading,
    formFieldValues,
    isEditItemLoading,
    isCreateItemLoading,
    isCreateItemSuccess,
    isEditItemSuccess,
    isDeleteItemLoading,
    isDeleteItemSuccess,
    item,
    vehicleList,
    leasingList,
    vehicleDetail,
    leasingDetail,
    fetchItemError,
    createItemError,
    editItemError,
    deleteItemError,
  }
}

const mapDispatchToProps = dispatch => ({
  createItem: params => dispatch(createItem(params)),
  editItem: params => dispatch(editItem(params)),
  fetchItem: params => dispatch(fetchItem(params)),
  getVehicleList: params => dispatch(getVehicleList(params)),
  getLeasingList: params => dispatch(getLeasingList(params)),
  getDetailLeasing: params => dispatch(getDetailLeasing(params)),
  getDetailVehicle: params => dispatch(getDetailVehicle(params)),
  deleteItem: params => dispatch(deleteItem(params)),
  editForm: formFieldsChange => dispatch(editForm(formFieldsChange)),
  reset: () => dispatch(reset()),
  resetLeasing: () => dispatch(resetLeasing()),
  resetVehicle: () => dispatch(resetVehicle()),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CreditSimulationForm))
