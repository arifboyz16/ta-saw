import React from 'react'
import { Card, Button } from 'antd'
import SectionContent from '@/components/SectionContent'

class MenuAdmin extends React.Component {
  render() {
    return (
      <SectionContent>
        <Card style={{ textAlign: 'center' }}>
          <Button
            type="primary"
            icon="user"
            size='large'
            style={{ marginLeft: '20px', marginTop: '10px' }}
            onClick={this.props.handleUser}
          > User </Button>
          <Button
            type="primary"
            icon="usergroup-add"
            size='large'
            style={{ marginLeft: '20px', marginTop: '10px' }}
            onClick={this.props.handleCustomer}
          > Customer </Button>
          </Card>
          <Card style={{ textAlign: 'center' }}>
          <Button
            type="default"
            icon="credit-card"
            size='large'
            style={{ marginLeft: '20px', marginTop: '10px' }}
            onClick={this.props.handleCreditDecision}
          > Credit Decision</Button>
          <Button
            type="default"
            icon="file-text"
            size='large'
            style={{ marginLeft: '20px', marginTop: '10px' }}
            onClick={this.props.handleCreditSimulation}
          > Credit Simulation</Button>
          </Card>
          <Card style={{ textAlign: 'center' }}>
          <Button
            type="danger"
            icon="car"
            size='large'
            style={{ marginLeft: '20px', marginTop: '10px' }}
            onClick={this.props.handleVehicle}
          > Vehicle</Button>
          <Button
            type="danger"
            icon="container"
            size='large'
            style={{ marginLeft: '20px', marginTop: '10px' }}
            onClick={this.props.handleLeasing}
          > Leasing</Button>
        </Card>
      </SectionContent>
    )
  }
}

export default MenuAdmin
