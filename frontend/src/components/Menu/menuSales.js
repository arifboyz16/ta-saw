import React from 'react'
import { Card, Button } from 'antd'
import SectionContent from '@/components/SectionContent'

class MenuSales extends React.Component {
  render() {
  	return (
		<SectionContent>
			<Card style={{ textAlign: 'center' }}>
				<Button
				type="default"
				icon="file-text"
				size='large'
				style={{ marginLeft: '20px', marginTop: '10px' }}
				onClick={this.props.handleCreditSimulation}
				> Credit Simulation</Button>
			</Card>
		</SectionContent>
	)
  }
}

export default MenuSales
