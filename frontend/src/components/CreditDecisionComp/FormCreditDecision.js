import React from 'react'
import { Form, Input, InputNumber, Select, Button, Modal } from 'antd'
import { BackButton } from '@/components/AppButton'

const FormItem = Form.Item
const { Option } = Select

class FormCreditDecision extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleOnDelete = this.handleOnDelete.bind(this)
    this.onChangeCustomer = this.onChangeCustomer.bind(this)
    this.onSearchCustomer = this.onSearchCustomer.bind(this)
    this.handleOnProces = this.handleOnProces.bind(this)
  }

  handleOnSubmit(e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSubmit(values)
    })
  }

  handleOnProces(){
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSAW(values)
    })
  }

  onChangeCustomer(value){
    this.props.onGetCustomerDetail(value)
  }

  onSearchCustomer(value) {
    this.props.onGetCustomerSearch(value)
  }

  listNumber() {
    let number = []
    let i
    for (i = 0; i < 36; i++) {
      number.push(i+1)
    }
    return number
  }

  handleOnDelete(e) {
    e.preventDefault()
    const handleOnOk = () => { this.props.onDelete() }
    Modal.confirm({
      title: 'Delete Item',
      content: 'Are you sure to delete this item?',
      okType: 'danger',
      onOk() {
        handleOnOk()
      },
      onCancel() {
        // console.log('Cancel')
      },
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 12,
          offset: 7,
        },
      },
    }
    const isCreateForm = this.props.type === 'create'
    const lisNumber = this.listNumber()
    return (
      <Form onSubmit={this.handleOnSubmit}>
        <Form.Item label="NIK" {...formItemLayout}>
          {getFieldDecorator('customer_id', {
            rules: [{ required: true, message: 'Please select your customer!' }],
          })(
            <Select
              showSearch
              onChange={this.onChangeCustomer}
              onSearch={this.onSearchCustomer}
              placeholder="Select customer"
            >
              {this.props.listCustomer.map((item) => (
                <Option value={item.id} key={item.id}>
                  {item.nik} - {item.name}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <FormItem label='Name' {...formItemLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <FormItem label='Address' {...formItemLayout}>
          {getFieldDecorator('address', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <FormItem label='No Handphone' {...formItemLayout}>
          {getFieldDecorator('handphone', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <FormItem label='Earning' {...formItemLayout}>
          {getFieldDecorator('earning', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <Form.Item label="House Status" {...formItemLayout}>
          {getFieldDecorator('house_status', {
            rules: [{ required: true, message: 'Please select house status!' }],
          })(
            <Select
              placeholder="Select house status"
            >
              <Option value="join">Join House</Option>
              <Option value="rent">Rent House</Option>
              <Option value="owned">Owned House</Option>
            </Select>,
          )}
        </Form.Item>
        <FormItem label='Other Income' {...formItemLayout}>
          {getFieldDecorator('other_income', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              style={{ width: '300px' }}
              placeholder='Other Income'
          />)}
        </FormItem>
        <Form.Item label="House Garage" {...formItemLayout}>
          {getFieldDecorator('house_garage', {
            rules: [{ required: true, message: 'Please select house garage!' }],
          })(
            <Select
              placeholder="Select house garage"
            >
              <Option value="nogarage">No Garage</Option>
              <Option value="rent">Rent</Option>
              <Option value="owned">Owned</Option>
            </Select>,
          )}
        </Form.Item>
        <FormItem label='Other Payment' {...formItemLayout}>
          {getFieldDecorator('other_payment', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              style={{ width: '300px' }}
              placeholder='Other Payment'
          />)}
        </FormItem>
        <Form.Item label="Credit History" {...formItemLayout}>
          {getFieldDecorator('credit_history', {
            rules: [{ required: true, message: 'Please select house garage!' }],
          })(
            <Select
              placeholder="Select credit history"
            >
              <Option value="bad">Bad</Option>
              <Option value="surveillance">Surveillance</Option>
              <Option value="newcomer">Newcomer</Option>
              <Option value="good">Good</Option>
            </Select>,
          )}
        </Form.Item>
        <FormItem label='Down Payment' {...formItemLayout}>
          {getFieldDecorator('down_payment', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              style={{ width: '300px' }}
              placeholder='Down Payment'
          />)}
        </FormItem>
        <Form.Item label="Period" {...formItemLayout}>
          {getFieldDecorator('periode', {
            rules: [{ required: true, message: 'Please select period!' }],
          })(
            <Select
              placeholder="Select period"
            >
              {lisNumber.map((item) => (
                <Option value={item} key={item}>
                  {item}
                </Option>
              ))}
            </Select>,
          )}
        </Form.Item>
        <FormItem label='Decision' {...formItemLayout}>
          {getFieldDecorator('decision', {
          })(<Input disabled />)}
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            onClick={this.handleOnProces}
            style={{ marginRight: '12px' }}
          >
            Process
          </Button>
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <BackButton
            onClick={this.props.BackButtonOnClick}
          />
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            htmlType='submit'
            style={{ marginRight: '12px' }}
          >
            Save
          </Button>
          {
            !isCreateForm &&
            <Button
              loading={this.props.isDeleteItemLoading}
              type='danger'
              htmlType='button'
              onClick={this.handleOnDelete}
            >
              Delete
            </Button>
          }
        </FormItem>
      </Form>
    )
  }
}

const CreditDecisionForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onFieldsChange(changedFields)
  },
  mapPropsToFields(props) {
    const { formFieldValues = {} } = props

    return {
      customer_id: Form.createFormField(formFieldValues.customer_id),
      name: Form.createFormField(formFieldValues.name),
      address: Form.createFormField(formFieldValues.address),
      handphone: Form.createFormField(formFieldValues.handphone),
      earning: Form.createFormField(formFieldValues.earning),
      house_status: Form.createFormField(formFieldValues.house_status),
      other_income: Form.createFormField(formFieldValues.other_income),
      house_garage: Form.createFormField(formFieldValues.house_garage),
      other_payment: Form.createFormField(formFieldValues.other_payment),
      credit_history: Form.createFormField(formFieldValues.credit_history),
      down_payment: Form.createFormField(formFieldValues.down_payment),
      periode: Form.createFormField(formFieldValues.periode),
      decision: Form.createFormField(formFieldValues.decision),
    }
  },
})(FormCreditDecision)

export default CreditDecisionForm
