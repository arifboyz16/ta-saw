import React from 'react'
import { Table, Modal } from 'antd'
import { TableRowEditButton, TableRowDeleteButton } from '@/components/AppButton'

class ListCreditDecision extends React.Component {

  render() {
    const columns = [
      {
        title: 'NIK',
        dataIndex: 'nik',
        key: 'nik',
        sorter: true,
      },
      {
        title: 'Nama Lengkap',
        dataIndex: 'name',
        key: 'name',
        sorter: true,
      }, {
        title: 'Alamat',
        dataIndex: 'address',
        key: 'address',
        sorter: true,
      }, {
        title: 'Decision',
        dataIndex: 'decision',
        key: 'decision',
        sorter: true,
      }, {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <TableRowEditButton to={`${this.props.editPath}/${record.id}`} />
            <TableRowDeleteButton onDelete={this.props.deleteData} id={record.id}/>
          </span>
        ),
      },
    ]
    return (
      <Table
        rowKey={record => `item-row-${record.id}`}
        columns={columns}
        {...this.props}
        scroll={{ x: 1000 }}
      />
    )
  }
}
ListCreditDecision
export default ListCreditDecision
