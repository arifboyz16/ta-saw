import React from 'react'
import { Form, Input, InputNumber, Select, Button, Modal } from 'antd'
import { BackButton } from '@/components/AppButton'

const FormItem = Form.Item
const { Option } = Select

class FormCustomer extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleOnDelete = this.handleOnDelete.bind(this)
  }

  handleOnSubmit(e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSubmit(values)
    })
  }

  handleOnDelete(e) {
    e.preventDefault()
    const handleOnOk = () => { this.props.onDelete() }
    Modal.confirm({
      title: 'Delete Item',
      content: 'Are you sure to delete this item?',
      okType: 'danger',
      onOk() {
        handleOnOk()
      },
      onCancel() {
        // console.log('Cancel')
      },
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 12,
          offset: 7,
        },
      },
    }
    const isCreateForm = this.props.type === 'create'
    return (
      <Form onSubmit={this.handleOnSubmit}>
        <FormItem label='NIK' {...formItemLayout}>
          {getFieldDecorator('nik', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='NIK' />)}
        </FormItem>
        <FormItem label='Name' {...formItemLayout}>
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Name' />)}
        </FormItem>
        <FormItem label='Address' {...formItemLayout}>
          {getFieldDecorator('address', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Address' />)}
        </FormItem>
        <FormItem label='No Handphone' {...formItemLayout}>
          {getFieldDecorator('handphone', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='No Handphone' />)}
        </FormItem>
        <FormItem label='Earning' {...formItemLayout}>
          {getFieldDecorator('earning', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              style={{ width: '300px' }}
              placeholder='Earning' 
          />)}
        </FormItem>
        <FormItem label='Tax Payer ID' {...formItemLayout}>
          {getFieldDecorator('tax_payer_id', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Tax Payer ID' />)}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <BackButton
            onClick={this.props.BackButtonOnClick}
          />
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            htmlType='submit'
            style={{ marginRight: '12px' }}
          >
            {isCreateForm ? 'Create' : 'Save'}
          </Button>
          {
            !isCreateForm &&
            <Button
              loading={this.props.isDeleteItemLoading}
              type='danger'
              htmlType='button'
              onClick={this.handleOnDelete}
            >
              Delete
            </Button>
          }
        </FormItem>
      </Form>
    )
  }
}

const CustomerForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onFieldsChange(changedFields)
  },
  mapPropsToFields(props) {
    const { formFieldValues = {} } = props
    return {
      nik: Form.createFormField(formFieldValues.nik),
      name: Form.createFormField(formFieldValues.name),
      address: Form.createFormField(formFieldValues.address),
      handphone: Form.createFormField(formFieldValues.handphone),
      earning: Form.createFormField(formFieldValues.earning),
      tax_payer_id: Form.createFormField(formFieldValues.tax_payer_id),
    }
  },
  // onValuesChange(_, values) {
  //   console.log(values)
  // },
})(FormCustomer)

export default CustomerForm
