import React from 'react'
import { Table, Modal } from 'antd'
import { TableRowEditButton, TableRowDeleteButton } from '@/components/AppButton'

class ListUser extends React.Component {

  render() {
    const columns = [
      {
        title: 'Nama Langkap',
        dataIndex: 'name',
        key: 'name',
        sorter: true,
      }, {
        title: 'Address',
        dataIndex: 'address',
        key: 'address',
        sorter: true,
      }, {
        title: 'No. Handphone',
        dataIndex: 'handphone',
        key: 'handphone',
        sorter: true,
      }, {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <TableRowEditButton to={`${this.props.editPath}/${record.id}`} />
            <TableRowDeleteButton onDelete={this.props.deleteData} id={record.id}/>
          </span>
        ),
      },
    ]
    return (
      <Table
        rowKey={record => `item-row-${record.id}`}
        columns={columns}
        {...this.props}
      />
    )
  }
}

export default ListUser
