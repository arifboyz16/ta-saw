import FormLeasing from './FormLeasing';
import ListLeasing from './ListLeasing';

export {
	FormLeasing,
	ListLeasing
};