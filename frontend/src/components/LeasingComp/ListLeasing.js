import React from 'react'
import { Table, Modal } from 'antd'
import { TableRowEditButton, TableRowDeleteButton } from '@/components/AppButton'

class ListLeasing extends React.Component {

  render() {
    const columns = [
      {
        title: 'Leasing Code',
        dataIndex: 'leasing_code',
        key: 'leasing_code',
        sorter: true,
      }, {
        title: 'Leasing Name',
        dataIndex: 'leasing_name',
        key: 'leasing_name',
        sorter: true,
      }, {
        title: 'Leasing Rate',
        dataIndex: 'leasing_rate',
        key: 'leasing_rate',
        sorter: true,
      }, {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <TableRowEditButton to={`${this.props.editPath}/${record.id}`} />
            <TableRowDeleteButton onDelete={this.props.deleteData} id={record.id}/>
          </span>
        ),
      },
    ]
    return (
      <Table
        rowKey={record => `item-row-${record.id}`}
        columns={columns}
        {...this.props}
        scroll={{ x: 1000 }}
      />
    )
  }
}

export default ListLeasing
