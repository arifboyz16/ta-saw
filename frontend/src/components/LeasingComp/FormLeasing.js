import React from 'react'
import { Form, Input, InputNumber, Select, Button, Modal } from 'antd'
import { BackButton } from '@/components/AppButton'

const FormItem = Form.Item
const { Option } = Select

class FormLeasing extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleOnDelete = this.handleOnDelete.bind(this)
  }

  handleOnSubmit(e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSubmit(values)
    })
  }

  handleOnDelete(e) {
    e.preventDefault()
    const handleOnOk = () => { this.props.onDelete() }
    Modal.confirm({
      title: 'Delete Item',
      content: 'Are you sure to delete this item?',
      okType: 'danger',
      onOk() {
        handleOnOk()
      },
      onCancel() {
        // console.log('Cancel')
      },
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 12,
          offset: 7,
        },
      },
    }
    const isCreateForm = this.props.type === 'create'
    return (
      <Form onSubmit={this.handleOnSubmit}>
        <Form.Item label="Leasing Code" {...formItemLayout}>
          {getFieldDecorator('leasing_code', {
            rules: [{ required: true, message: 'Please select leasing code!' }],
          })(<Input placeholder='Leasing Code' />)}
        </Form.Item>
        <FormItem label='Leasing Name' {...formItemLayout}>
          {getFieldDecorator('leasing_name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Leasing Name' />)}
        </FormItem>
        <FormItem label='Leasing Rate' {...formItemLayout}>
          {getFieldDecorator('leasing_rate', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              formatter={leasing_rate => `${leasing_rate}`.replace(/\B(?=(\d{3})+(?!\d))/g, '.')}
              parser={leasing_rate => leasing_rate.replace(/\$\s?|(,*)/g, '')}
            />)}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <BackButton
            onClick={this.props.BackButtonOnClick}
          />
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            htmlType='submit'
            style={{ marginRight: '12px' }}
          >
            {isCreateForm ? 'Create' : 'Save'}
          </Button>
          {
            !isCreateForm &&
            <Button
              loading={this.props.isDeleteItemLoading}
              type='danger'
              htmlType='button'
              onClick={this.handleOnDelete}
            >
              Delete
            </Button>
          }
        </FormItem>
      </Form>
    )
  }
}

const LeasingForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onFieldsChange(changedFields)
  },
  mapPropsToFields(props) {
    const { formFieldValues = {} } = props
    return {
      leasing_code: Form.createFormField(formFieldValues.leasing_code),
      leasing_name: Form.createFormField(formFieldValues.leasing_name),
      leasing_rate: Form.createFormField(formFieldValues.leasing_rate),
    }
  },
})(FormLeasing)

export default LeasingForm
