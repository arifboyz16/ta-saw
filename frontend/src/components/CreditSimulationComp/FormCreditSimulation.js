import React from 'react'
import { Form, Input, InputNumber, Select, Button, Modal } from 'antd'
import { BackButton } from '@/components/AppButton'

const FormItem = Form.Item
const { Option } = Select

class FormCreditSimulation extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.onChangeVehicle = this.onChangeVehicle.bind(this)
    this.onChangeLeasing = this.onChangeLeasing.bind(this)
  }

  handleOnSubmit(e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSubmit(values)
    })
  }

  onChangeVehicle(value){
    this.props.onGetVehicleDetail(value)
  }

  onChangeLeasing(value){
    this.props.onGetLeasingDetail(value)
  }

  listNumber() {
    let number = []
    let i
    for (i = 0; i < 36; i++) {
      number.push(i+1)
    }
    return number
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 12,
          offset: 7,
        },
      },
    }
    const isCreateForm = this.props.type === 'create'
    const lisNumber = this.listNumber()

    return (
      <Form onSubmit={this.handleOnSubmit}>
        <Form.Item label="Vehicle Code" {...formItemLayout}>
          {getFieldDecorator('vehicle_code', {
            rules: [{ required: true, message: 'Please select your position!' }],
          })(
            <Select
              onChange={this.onChangeVehicle}
              placeholder="Select vehicle code"
            >
              {this.props.listVehicle.map((item) => (
                <Option value={item.id} key={item.id}>
                  {item.vehicle_code}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <FormItem label='Vehicle Name' {...formItemLayout}>
          {getFieldDecorator('vehicle_name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <FormItem label='Vehicle Type' {...formItemLayout}>
          {getFieldDecorator('vehicle_type', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <FormItem label='Vehicle Price' {...formItemLayout}>
          {getFieldDecorator('vehicle_price', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <Form.Item label="Leasing Code" {...formItemLayout}>
          {getFieldDecorator('leasing_code', {
            rules: [{ required: true, message: 'Please select leasing code!' }],
          })(
            <Select
              onChange={this.onChangeLeasing}
              placeholder="Select leasing code"
            >
              {this.props.listLeasing.map((item) => (
                <Option value={item.id} key={item.id}>
                  {item.leasing_code}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>
        <FormItem label='Leasing Name' {...formItemLayout}>
          {getFieldDecorator('leasing_name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <FormItem label='Leasing Rate' {...formItemLayout}>
          {getFieldDecorator('leasing_rate', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input disabled />)}
        </FormItem>
        <Form.Item label="Period" {...formItemLayout}>
          {getFieldDecorator('period', {
            rules: [{ required: true, message: 'Please select period!' }],
          })(
            <Select
              placeholder="Select period"
            >
              {lisNumber.map((item) => (
                <Option value={item} key={item}>
                  {item}
                </Option>
              ))}
            </Select>,
          )}
        </Form.Item>
        <FormItem label='Down Payment' {...formItemLayout}>
          {getFieldDecorator('down_payment', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              style={{ width: '300px' }}
              placeholder='Down Payment' 
          />)}
        </FormItem>
        <FormItem label='Installment' {...formItemLayout}>
          {getFieldDecorator('installment', {
          })(<Input disabled />)}
        </FormItem>
        <FormItem {...tailFormItemLayout}>
          <BackButton
            onClick={this.props.BackButtonOnClick}
          />
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            htmlType='submit'
            style={{ marginRight: '12px' }}
          >
            Process
          </Button>
        </FormItem>
      </Form>
    )
  }
}

const CreditSimulationForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onFieldsChange(changedFields)
  },
  mapPropsToFields(props) {
    const { formFieldValues = {} } = props
    return {
      vehicle_code: Form.createFormField(formFieldValues.vehicle_code),
      vehicle_name: Form.createFormField(formFieldValues.vehicle_name),
      vehicle_type: Form.createFormField(formFieldValues.vehicle_type),
      vehicle_price: Form.createFormField(formFieldValues.vehicle_price),
      leasing_code: Form.createFormField(formFieldValues.leasing_code),
      leasing_name: Form.createFormField(formFieldValues.leasing_name),
      leasing_rate: Form.createFormField(formFieldValues.leasing_rate),
      period: Form.createFormField(formFieldValues.period),
      down_payment: Form.createFormField(formFieldValues.down_payment),
      installment: Form.createFormField(formFieldValues.installment),
    }
  },
})(FormCreditSimulation)

export default CreditSimulationForm
