import React from 'react'
import { Form, Input, InputNumber, Select, Button, Modal } from 'antd'
import { BackButton } from '@/components/AppButton'

const FormItem = Form.Item
const { Option } = Select

class FormVehicle extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleOnDelete = this.handleOnDelete.bind(this)
  }

  handleOnSubmit(e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSubmit(values)
    })
  }

  handleOnDelete(e) {
    e.preventDefault()
    const handleOnOk = () => { this.props.onDelete() }
    Modal.confirm({
      title: 'Delete Item',
      content: 'Are you sure to delete this item?',
      okType: 'danger',
      onOk() {
        handleOnOk()
      },
      onCancel() {
        // console.log('Cancel')
      },
    })
  }

  checkPrice = (rule, value, callback) => {
    if (value.number > 0) {
      callback();
      return;
    }
    callback('Price must greater than zero!');
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 12,
          offset: 7,
        },
      },
    }
    const isCreateForm = this.props.type === 'create'

    return (
      <Form onSubmit={this.handleOnSubmit}>
        <Form.Item label="Vehicle Code" {...formItemLayout}>
          {getFieldDecorator('vehicle_code', {
            rules: [{ required: true, message: 'Please select your position!' }],
          })(<Input placeholder='Vehicle Code' />)}
        </Form.Item>
        <FormItem label='Vehicle Name' {...formItemLayout}>
          {getFieldDecorator('vehicle_name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Vehicle Name' />)}
        </FormItem>
        <FormItem label='Vehicle Type' {...formItemLayout}>
          {getFieldDecorator('vehicle_type', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Vehicle Type' />)}
        </FormItem>
        <FormItem label='Vehicle Price' {...formItemLayout}>
          {getFieldDecorator('vehicle_price', {
            rules: [{ required: true, type: 'number', message: 'Please input a valid value.' }],
          })(<InputNumber
              style={{ width: '300px' }}
              placeholder='Vehicle Price'
          />)}
        </FormItem>

        <FormItem {...tailFormItemLayout}>
          <BackButton
            onClick={this.props.BackButtonOnClick}
          />
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            htmlType='submit'
            style={{ marginRight: '12px' }}
          >
            {isCreateForm ? 'Create' : 'Save'}
          </Button>
          {
            !isCreateForm &&
            <Button
              loading={this.props.isDeleteItemLoading}
              type='danger'
              htmlType='button'
              onClick={this.handleOnDelete}
            >
              Delete
            </Button>
          }
        </FormItem>
      </Form>
    )
  }
}

const VehicleForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onFieldsChange(changedFields)
  },
  mapPropsToFields(props) {
    const { formFieldValues = {} } = props
    return {
      vehicle_code: Form.createFormField(formFieldValues.vehicle_code),
      vehicle_name: Form.createFormField(formFieldValues.vehicle_name),
      vehicle_type: Form.createFormField(formFieldValues.vehicle_type),
      vehicle_price: Form.createFormField(formFieldValues.vehicle_price),
    }
  },
})(FormVehicle)

export default VehicleForm
