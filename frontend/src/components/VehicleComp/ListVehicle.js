import React from 'react'
import { Table, Modal } from 'antd'
import { TableRowEditButton, TableRowDeleteButton } from '@/components/AppButton'

class ListVehicle extends React.Component {

  render() {
    const columns = [
      {
        title: 'Vehicle Code',
        dataIndex: 'vehicle_code',
        key: 'vehicle_code',
        sorter: true,
      }, {
        title: 'Vehicle Name',
        dataIndex: 'vehicle_name',
        key: 'vehicle_name',
        sorter: true,
      }, {
        title: 'Vehicle Type',
        dataIndex: 'vehicle_type',
        key: 'vehicle_type',
        sorter: true,
      }, {
        title: 'Vehicle Price',
        dataIndex: 'vehicle_price',
        key: 'vehicle_price',
        sorter: true,
      }, {
        title: 'Action',
        key: 'action',
        render: (text, record) => (
          <span>
            <TableRowEditButton to={`${this.props.editPath}/${record.id}`} />
            <TableRowDeleteButton onDelete={this.props.deleteData} id={record.id}/>
          </span>
        ),
      },
    ]
    return (
      <Table
        rowKey={record => `item-row-${record.id}`}
        columns={columns}
        {...this.props}
        scroll={{ x: 1000 }}
      />
    )
  }
}

export default ListVehicle
