import React from 'react'
import { Form, Input, Select, Button, Modal } from 'antd'
import { BackButton } from '@/components/AppButton'

const FormItem = Form.Item
const { Option } = Select

class FormUser extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
    this.handleOnDelete = this.handleOnDelete.bind(this)
  }

  handleOnSubmit(e) {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        return
      }
      this.props.onSubmit(values)
    })
  }

  handleOnDelete(e) {
    e.preventDefault()
    const handleOnOk = () => { this.props.onDelete() }
    Modal.confirm({
      title: 'Delete Item',
      content: 'Are you sure to delete this item?',
      okType: 'danger',
      onOk() {
        handleOnOk()
      },
      onCancel() {
        // console.log('Cancel')
      },
    })
  }

  render() {
    const { getFieldDecorator } = this.props.form
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 7 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    }
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 12,
          offset: 7,
        },
      },
    }
    const isCreateForm = this.props.type === 'create'
    return (
      <Form onSubmit={this.handleOnSubmit}>
        <FormItem label='NRP' {...formItemLayout}>
          {getFieldDecorator('nrp', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='NRP' />)}
        </FormItem>
        <FormItem label='Email' {...formItemLayout}>
          {getFieldDecorator('email', {
            rules: [{ required: true, type: 'email', message: 'Please input a valid value.' }],
          })(<Input placeholder='Email' />)}
        </FormItem>
        <FormItem label='Username' {...formItemLayout}>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Username' />)}
        </FormItem>
        <FormItem label='First Name' {...formItemLayout}>
          {getFieldDecorator('first_name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='First Name' />)}
        </FormItem>
        <FormItem label='Last Name' {...formItemLayout}>
          {getFieldDecorator('last_name', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='Last Name' />)}
        </FormItem>
        <FormItem label='Password' {...formItemLayout}>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input a valid password.' }],
          })(<Input placeholder='Password' type='password' />)}
        </FormItem>
        <FormItem label='No Handphone' {...formItemLayout}>
          {getFieldDecorator('handphone', {
            rules: [{ required: true, message: 'Please input a valid value.' }],
          })(<Input placeholder='No Handphone' />)}
        </FormItem>
        <Form.Item label="Position" {...formItemLayout}>
          {getFieldDecorator('posisi', {
            rules: [{ required: true, message: 'Please select your position!' }],
          })(
            <Select
              placeholder="Select a option and change input text above"
            >
              <Option value="sales_supervisor">Sales Supervisor</Option>
              <Option value="sales">Sales</Option>
            </Select>,
          )}
        </Form.Item>
        <FormItem {...tailFormItemLayout}>
          <BackButton
            onClick={this.props.BackButtonOnClick}
          />
          <Button
            loading={this.props.isCreateItemLoading || this.props.isEditItemLoading}
            type='primary'
            htmlType='submit'
            style={{ marginRight: '12px' }}
          >
            {isCreateForm ? 'Create' : 'Save'}
          </Button>
          {
            !isCreateForm &&
            <Button
              loading={this.props.isDeleteItemLoading}
              type='danger'
              htmlType='button'
              onClick={this.handleOnDelete}
            >
              Delete
            </Button>
          }
        </FormItem>
      </Form>
    )
  }
}

const UserForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onFieldsChange(changedFields)
  },
  mapPropsToFields(props) {
    const { formFieldValues = {} } = props
    return {
      nrp: Form.createFormField(formFieldValues.nrp),
      email: Form.createFormField(formFieldValues.email),
      username: Form.createFormField(formFieldValues.username),
      first_name: Form.createFormField(formFieldValues.first_name),
      last_name: Form.createFormField(formFieldValues.last_name),
      password: Form.createFormField(formFieldValues.password),
      handphone: Form.createFormField(formFieldValues.handphone),
      posisi: Form.createFormField(formFieldValues.posisi),
    }
  },
  // onValuesChange(_, values) {
  //   console.log(values)
  // },
})(FormUser)

export default UserForm
