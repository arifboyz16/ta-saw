import React from 'react'
import { Layout } from 'antd'
import './index.less'

const { Footer } = Layout

class CustomisedFooter extends React.Component {
  render() {
    return (
      <Footer className='app-footer'>
        @2019
      </Footer>
    )
  }
}

export default CustomisedFooter
