import React from 'react'
import { connect } from 'react-redux'
import { Layout, Menu, Icon } from 'antd'
import { withRouter } from 'react-router-dom'
import Header from './Header'
import AppFooter from '@/components/AppFooter'
import logo from '@/assets/react.svg'
import { openChangeMenu } from '@/actions/admin'
import './index.less'

const { Sider, Content } = Layout
const { SubMenu } = Menu

class Admin extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.handleHeaderOnToggle = this.handleHeaderOnToggle.bind(this)
    this.handleMenuItemOnClick = this.handleMenuItemOnClick.bind(this)
    this.state = {
      collapsed: false,
    }
  }

  handleHeaderOnToggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    })
  }

  handleMenuItemOnClick(item) {
    this.props.history.push(`/admin/${item.key}`)
  }

  render() {
    const { match, children, openKeys } = this.props
    const selectedKey = match.path.split('/').splice(-1)[0]
    return (
      <Layout className='admin-page'>
        <Layout>
          <Header collapsed={this.state.collapsed} handleHeaderOnToggle={this.handleHeaderOnToggle} />
          <Content>
            {children}
          </Content>
          <AppFooter />
        </Layout>
      </Layout>
    )
  }
}

const mapStateToProps = (state) => {
  const { openKeys } = state.admin || ['general']
  return { openKeys }
}

const mapDispatchToProps = dispatch => ({
  handleMenuOnOpenChange: openKeys => dispatch(openChangeMenu(openKeys)),
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Admin))
